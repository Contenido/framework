module gitlab.com/Contenido/framework

go 1.15

require (
	github.com/ClickHouse/clickhouse-go v1.4.3
	github.com/gofrs/uuid v3.3.0+incompatible
	github.com/jackc/pgproto3/v2 v2.0.7 // indirect
	github.com/jackc/pgx/v4 v4.10.1
	github.com/jmoiron/sqlx v1.3.1
	github.com/labstack/echo-contrib v0.9.0
	github.com/labstack/echo/v4 v4.1.17
	github.com/prometheus/client_golang v1.8.0
	github.com/rs/zerolog v1.20.0
	github.com/shopspring/decimal v1.2.0 // indirect
	github.com/streadway/amqp v1.0.0
	github.com/vrischmann/envconfig v1.3.0
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83 // indirect
	golang.org/x/text v0.3.5 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)
