package framework

// InternalHTTPServer is a name for internal HTTP server provider that can be
// used by different framework parts.
const InternalHTTPServer = "internal_http_server"
