package pubsub

import (
	"context"
	"fmt"

	"gitlab.com/Contenido/framework/errors"
	"gitlab.com/Contenido/framework/internal/handler"
)

// Handler is a PubSub handling control structure.
type Handler struct {
	handler.BaseHandler
}

// CreateTopic creates new topic or returns an error if topic is already created.
// Only topic name is used for searching, so if you want to use same topic name but
// different distribution type - consider create properly named topic (e.g.
// "topic1_fanout" for fanout topic and "topic1_direct" for directs).
// For sending messages use Send().
func (h *Handler) CreateTopic(
	providerName,
	topicName string,
	distributionType Type,
	lbType LoadBalancingStrategy,
) error {
	provider, err := h.GetProvider(providerName)
	if err != nil {
		return err
	}

	return provider.CreateTopic(topicName, distributionType, lbType)
}

// CreateTopicWithContext is the very same as CreateTopic but also sets passed context
// to topic.
func (h *Handler) CreateTopicWithContext(
	providerName,
	topicName string,
	distributionType Type,
	lbType LoadBalancingStrategy,
	ctx context.Context,
) error {
	provider, err := h.GetProvider(providerName)
	if err != nil {
		return err
	}

	return provider.CreateTopicWithContext(topicName, distributionType, lbType, ctx)
}

// GetProvider returns provider by name.
func (h *Handler) GetProvider(providerName string) (Provider, error) {
	providerRaw, err := h.GetRawProvider(providerName)
	if err != nil {
		return nil, fmt.Errorf("pubsub provider: %w", err)
	}

	provider, ok := providerRaw.(Provider)
	if !ok {
		return nil, errors.ErrInvalidProvider
	}

	return provider, nil
}

// Initialize initializes internal state.
func (h *Handler) Initialize() {}

// Send sends message to topic in designated provider.
func (h *Handler) Send(providerName string, msg *Message) (bool, error) {
	provider, err := h.GetProvider(providerName)
	if err != nil {
		return false, err
	}

	return provider.Send(msg)
}

// StopTopic stops topic.
func (h *Handler) StopTopic(providerName, topicName string) error {
	provider, err := h.GetProvider(providerName)
	if err != nil {
		return err
	}

	return provider.StopTopic(topicName)
}

// Subscribe subscribes to topic for designated provider.
func (h *Handler) Subscribe(providerName, topicName string) (*Channel, error) {
	provider, err := h.GetProvider(providerName)
	if err != nil {
		return nil, err
	}

	return provider.Subscribe(topicName)
}
