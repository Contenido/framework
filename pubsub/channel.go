package pubsub

import "sync"

// Channel is a generic channel controlling structure. It is used for both
// receiving and sending channels.
type Channel struct {
	channel chan *Message

	channelClosed      bool
	channelClosedMutex sync.RWMutex
}

func NewChannel() *Channel {
	return &Channel{
		channel: make(chan *Message),
	}
}

// Close closes channel.
func (c *Channel) Close() {
	c.channelClosedMutex.Lock()
	defer c.channelClosedMutex.Unlock()
	if !c.channelClosed {
		c.channelClosed = true

		close(c.channel)
		c.channel = nil
	}
}

// GetChannel returns underlying channel if it wasn't closed. Returns nil if channel was closed.
func (c *Channel) GetChannel() chan *Message {
	c.channelClosedMutex.RLock()
	defer c.channelClosedMutex.RUnlock()

	if !c.channelClosed {
		return c.channel
	}

	return nil
}

// IsClosed returns true if channel was closed.
func (c *Channel) IsClosed() bool {
	c.channelClosedMutex.RLock()
	defer c.channelClosedMutex.RUnlock()

	return c.channelClosed
}

// Send sends message thru channel. If channel was closed message will be
// silently ignored and function returns false. Returns true if message was sent.
func (c *Channel) Send(msg *Message) bool {
	var sent bool

	c.channelClosedMutex.RLock()
	defer c.channelClosedMutex.RUnlock()

	if !c.channelClosed {
		c.channel <- msg

		sent = true
	}

	return sent
}
