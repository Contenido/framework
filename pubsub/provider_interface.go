package pubsub

import (
	"context"

	"gitlab.com/Contenido/framework/internal/provider"
)

type Provider interface {
	provider.BaseInterface

	// CreateTopic creates new topic or returns an error if topic is already created.
	// Only topic name is used for searching, so if you want to use same topic name but
	// different distribution type - consider create properly named topic (e.g.
	// "topic1_fanout" for fanout topic and "topic1_direct" for directs).
	// For sending messages use Send().
	CreateTopic(topicName string, topicType Type, lbStrategy LoadBalancingStrategy) error
	// CreateTopicWithContext is the very same as CreateTopic but also sets
	// passed context to topic.
	CreateTopicWithContext(topicName string, topicType Type, lbStrategy LoadBalancingStrategy, ctx context.Context) error
	// Send sends message. Returns true as first parameter if message was successfully
	// sent and false if isn't.
	Send(msg *Message) (bool, error)
	// StopTopic stops topic and all activity on it.
	StopTopic(topicName string) error
	// Subscribe returns a structure that contains channel which is used
	// for receiving messages (effectively "subscribing" to topic).
	Subscribe(topicName string) (*Channel, error)
}
