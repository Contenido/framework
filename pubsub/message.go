package pubsub

// Message is a structure that should be sent to subscribers.
type Message struct {
	Topic   string
	Message interface{}
}
