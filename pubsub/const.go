package pubsub

const (
	// TypeDirect represents direct pub-sub type, when message is delivered
	// to only one connected subscriber.
	TypeDirect Type = "direct"
	// TypeFanout represents fan-out pub-sub type, when message is delivered
	// to all connected subscribers.
	TypeFanout Type = "fanout"
)

// PubSubType is a type of pub-sub topic.
type Type string

// LoadBalancingStrategy represents type of load balancing strategy to be used
// by provider when creating "direct" topic.
type LoadBalancingStrategy string

const (
	// LoadBalancingStrategyLeastSent is a strategy that takes in count number of
	// sent messages to channel.
	LoadBalancingStrategyLeastSent LoadBalancingStrategy = "least_sent"
	// LoadBalancingStrategyRandom is a fully random load balancing strategy.
	LoadBalancingStrategyRandom LoadBalancingStrategy = "random"
	// LoadBalancingStrategyUnknown is an unknown load balancing strategy. Set by default.
	LoadBalancingStrategyUnknown LoadBalancingStrategy = "unknown"
)
