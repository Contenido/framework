package simple

import "gitlab.com/Contenido/framework/pubsub"

// Function signature that provides load balancing feature.
type loadBalancer func() *channel

// Sets load balancing strategy.
func (t *topic) setLoadBalancingStrategy(strategy pubsub.LoadBalancingStrategy) {
	switch strategy {
	case pubsub.LoadBalancingStrategyLeastSent:
		t.balancer = t.loadBalancerLeastSent
	case pubsub.LoadBalancingStrategyRandom:
		t.balancer = t.loadBalancerRandom
	case pubsub.LoadBalancingStrategyUnknown:
		t.balancer = t.loadBalancerUnknown
	}
}

// Returns a channel with least sent messages. If all channels have equal number of
// sent messages - returns first from list of channels.
func (t *topic) loadBalancerLeastSent() *channel {
	t.outgoingMutex.RLock()
	defer t.outgoingMutex.RUnlock()

	// Return immediately if only one subscriber is present.
	if len(t.outgoing) == 1 {
		return t.outgoing[0]
	}

	// By default we will use first subscriber channel's data.
	channelToReturn := t.outgoing[0]
	lastSentCount := channelToReturn.GetItemsSentCount()

	// Check all other channels.
	for _, channel := range t.outgoing {
		sentToChannel := channel.GetItemsSentCount()
		if sentToChannel >= lastSentCount {
			lastSentCount = sentToChannel

			continue
		}

		channelToReturn = channel
	}

	return channelToReturn
}

// Returns completely random channel from list of channels.
func (t *topic) loadBalancerRandom() *channel {
	t.outgoingMutex.RLock()
	defer t.outgoingMutex.RUnlock()

	// Do not panic if we have no subscribers yet.
	if len(t.outgoing) == 0 {
		t.logger.Logf("warn",
			"Trying to send a message to random subscriber without subscribers registered. Message will not be delivered.",
			t.loggerAdditionals)

		return nil
	}

	return t.outgoing[t.randomer.Int63n(int64(len(t.outgoing)))]
}

// Prints an error to logs.
func (t *topic) loadBalancerUnknown() *channel {
	t.logger.Log("error",
		"Trying to send a message to queue without load balancing strategy set! Message will not be sent!",
		t.loggerAdditionals,
	)

	return nil
}
