package simple

import (
	"sync"

	"gitlab.com/Contenido/framework/pubsub"
)

// This is a wrapper for default channel that provide data required by provider.
type channel struct {
	channel *pubsub.Channel

	messagesSent      uint64 // Count of messages that was sent to channel.
	messagesSentMutex sync.RWMutex
}

func newChannel() *channel {
	return &channel{
		channel:           pubsub.NewChannel(),
		messagesSent:      uint64(0),
		messagesSentMutex: sync.RWMutex{},
	}
}

func (c *channel) GetItemsSentCount() uint64 {
	c.messagesSentMutex.RLock()
	defer c.messagesSentMutex.RUnlock()

	return c.messagesSent
}

func (c *channel) Send(msg *pubsub.Message) bool {
	sent := c.channel.Send(msg)
	if sent {
		c.messagesSentMutex.Lock()
		c.messagesSent++
		c.messagesSentMutex.Unlock()
	}

	return sent
}
