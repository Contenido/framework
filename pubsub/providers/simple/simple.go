package simple

import (
	"context"
	"fmt"
	"sync"

	"gitlab.com/Contenido/framework"
	"gitlab.com/Contenido/framework/errors"
	"gitlab.com/Contenido/framework/internal/provider"
	"gitlab.com/Contenido/framework/logging"
	"gitlab.com/Contenido/framework/pubsub"
)

var defaultProviderAdditionals = map[string]interface{}{"name": "simple pubsub provider"}

// Provider is a simple pub-sub provider. It provides nothing else than channels-based pub-sub.
type Provider struct {
	app    *framework.Application
	logger logging.Provider
	topics map[string]*topic
	provider.BaseProvider
	topicsMutex sync.RWMutex
}

// NewProvider creates new simple pub-sub provider.
func NewProvider(a *framework.Application) *Provider {
	// As this is a provider instantiation there is no actual possibility to fill all
	// fields (they'll be filled with Initialize() call), so:
	// nolint:exhaustivestruct
	return &Provider{
		app: a,
	}
}

// Really creates topic.
func (p *Provider) createTopic(
	name string,
	distributionType pubsub.Type,
	lbType pubsub.LoadBalancingStrategy,
	ctx context.Context,
) error {
	p.logger.Logf("debug", "Creating new topic: '%s'", defaultProviderAdditionals, name)

	_, err := p.getTopic(name)
	if err == nil {
		p.logger.Logf("error", "Cannot create topic '%s' - already exists", defaultProviderAdditionals, name)

		return errors.ErrPubSubTopicAlreadyExists
	}

	t, err := newTopic(name, distributionType, lbType, p.app, ctx)
	if err != nil {
		p.logger.Logf("error", "Failed to create topic for queue '%s': %s", defaultProviderAdditionals, name, err.Error())

		return err
	}

	p.topicsMutex.Lock()
	p.topics[name] = t
	p.topicsMutex.Unlock()

	if p.IsStarted() {
		p.StartTopic(t.name)
	}

	p.logger.Logf("debug", "Topic '%s' created", defaultProviderAdditionals, name)

	return nil
}

// CreateTopic creates new topic or returns an error if topic is already created.
// Only topic name is used for searching, so if you want to use same topic name but
// different distribution type - consider create properly named topic (e.g.
// "topic1_fanout" for fanout topic and "topic1_direct" for directs).
// For sending messages use Send().
func (p *Provider) CreateTopic(name string, distributionType pubsub.Type, lbType pubsub.LoadBalancingStrategy) error {
	return p.createTopic(name, distributionType, lbType, p.GetContext())
}

// CreateTopicWithContext is same as CreateTopic but uses passed context as topic's
// context.
func (p *Provider) CreateTopicWithContext(
	name string,
	distributionType pubsub.Type,
	lbType pubsub.LoadBalancingStrategy,
	ctx context.Context,
) error {
	return p.createTopic(name, distributionType, lbType, ctx)
}

// Deletes topic from a list of known topics.
func (p *Provider) deleteTopic(topicName string) {
	p.logger.Logf("debug", "Deleting topic '%s' from known topics...", defaultProviderAdditionals, topicName)

	p.topicsMutex.Lock()
	defer p.topicsMutex.Unlock()

	delete(p.topics, topicName)
}

// Returns topic by name or error if it wasn't found.
func (p *Provider) getTopic(topicName string) (*topic, error) {
	p.logger.Logf("debug", "Getting topic '%s'", defaultProviderAdditionals, topicName)

	p.topicsMutex.RLock()
	defer p.topicsMutex.RUnlock()

	topic, found := p.topics[topicName]
	if !found {
		return nil, errors.ErrPubSubTopicNotFound
	}

	return topic, nil
}

// Initialize initializes provider's internal state.
func (p *Provider) Initialize() error {
	logProvider, err := p.app.Logger.GetProvider(logging.DefaultLoggingProvider)
	if err != nil {
		return fmt.Errorf("simple pubsub provider: %w", err)
	}

	p.logger = logProvider

	p.logger.Logf("info", "Initializing provider...", defaultProviderAdditionals)

	p.topics = make(map[string]*topic)

	p.RegisterStartFunc(p.startTopics)
	p.RegisterStopFunc(p.stopTopics)

	return nil
}

// SetConfig sets provider's configuration. But right not there are no configuration
// possible for this provider.
func (p *Provider) SetConfig(config interface{}) error { return nil }

// Send sends message.
func (p *Provider) Send(msg *pubsub.Message) (bool, error) {
	p.logger.Logf("debug", "Sending message: %+v", defaultProviderAdditionals, msg)

	if msg.Topic == "" {
		return false, errors.ErrPubSubMessageTopicUndefined
	}

	topic, err := p.getTopic(msg.Topic)
	if err != nil {
		return false, err
	}

	return topic.send(msg), nil
}

// StartTopic starts topic handling goroutines.
func (p *Provider) StartTopic(topicName string) error {
	p.logger.Logf("debug", "Starting worker for topic '%s'...", defaultProviderAdditionals, topicName)

	topic, err := p.getTopic(topicName)
	if err != nil {
		return err
	}

	topic.start()

	go p.stopTopicListener(topic)

	return nil
}

// Starts topics handling goroutines. It is just a wrapper around StartTopic which
// executes on provider's start.
func (p *Provider) startTopics() error {
	p.logger.Log("debug", "Starting workers for all known topics...", defaultProviderAdditionals)

	p.topicsMutex.RLock()

	topics := make([]string, 0, len(p.topics))

	for topicName := range p.topics {
		topics = append(topics, topicName)
	}

	p.topicsMutex.RUnlock()

	for _, topicName := range topics {
		err := p.StartTopic(topicName)
		if err != nil {
			return err
		}
	}

	return nil
}

// StopTopic stops topic handling goroutines.
func (p *Provider) StopTopic(topicName string) error {
	p.logger.Logf("debug", "Stopping worker for topic '%s'...", defaultProviderAdditionals, topicName)

	topic, err := p.getTopic(topicName)
	if err != nil {
		return err
	}

	topic.stop()

	p.deleteTopic(topicName)

	return nil
}

// Listens to topic's context.Context to properly delete topic.
func (p *Provider) stopTopicListener(t *topic) {
	p.logger.Logf("debug", "Topic stop listener for topic '%s' started", defaultProviderAdditionals, t.name)

	<-t.ctx.Done()
	p.logger.Logf(
		"debug",
		"Topic '%s' received stop signal, deleting from a list of known topics...",
		defaultProviderAdditionals,
		t.name,
	)

	p.deleteTopic(t.name)
}

// Stops topics handling goroutines. It is just a wrapper around StopTopic which
// executes on provider's stop.
func (p *Provider) stopTopics() error {
	p.logger.Logf("debug", "Stopping workers for all known topics...", defaultProviderAdditionals)

	p.topicsMutex.RLock()

	topics := make([]string, 0, len(p.topics))

	for topicName := range p.topics {
		topics = append(topics, topicName)
	}

	p.topicsMutex.RUnlock()

	for _, topicName := range topics {
		err := p.StopTopic(topicName)
		if err != nil {
			return err
		}
	}

	return nil
}

// Subscribe returns new channel for specified topic effectively "subscribing" to it.
func (p *Provider) Subscribe(topicName string) (*pubsub.Channel, error) {
	p.logger.Logf("debug", "Creating subscription channel for topic '%s'...", defaultProviderAdditionals, topicName)

	topic, err := p.getTopic(topicName)
	if err != nil {
		return nil, err
	}

	channel := topic.subscribe()

	return channel.channel, nil
}
