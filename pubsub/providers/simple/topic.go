package simple

import (
	"context"
	"fmt"
	"math/rand"
	"sync"
	"time"

	"gitlab.com/Contenido/framework"
	"gitlab.com/Contenido/framework/logging"
	"gitlab.com/Contenido/framework/pubsub"
)

// Topic controller. Each topic starts separate goroutine that will rule incoming
// messages and distribute them to subscribers.
type topic struct {
	ctx                 context.Context
	cancelFunc          context.CancelFunc
	logger              logging.Provider
	loggerAdditionals   map[string]interface{}
	balancer            loadBalancer
	app                 *framework.Application
	incoming            *channel
	randomer            *rand.Rand
	name                string
	distributionType    pubsub.Type
	loadBalanceStrategy pubsub.LoadBalancingStrategy
	outgoing            []*channel
	outgoingMutex       sync.RWMutex
	workWaiter          sync.WaitGroup
}

// Creates new topic.
func newTopic(
	name string,
	distributionType pubsub.Type,
	lbType pubsub.LoadBalancingStrategy,
	a *framework.Application,
	ctx context.Context,
) (*topic, error) {
	// As this is a topic instantiation there is no actual possibility to fill all
	// fields (they'll be filled with initialize() call), so:
	// nolint:exhaustivestruct
	t := &topic{
		app:                 a,
		ctx:                 ctx,
		distributionType:    distributionType,
		loadBalanceStrategy: lbType,
		name:                name,
	}

	err := t.initialize()
	if err != nil {
		return nil, err
	}

	t.setLoadBalancingStrategy(lbType)

	return t, nil
}

// Initializes topic's internal state.
func (t *topic) initialize() error {
	logProvider, err := t.app.Logger.GetProvider(logging.DefaultLoggingProvider)
	if err != nil {
		return fmt.Errorf("simple pubsub provider: topic initialization: get logger: %w", err)
	}

	t.logger = logProvider

	t.loggerAdditionals = make(map[string]interface{})

	for k, v := range defaultProviderAdditionals {
		t.loggerAdditionals[k] = v
	}

	t.loggerAdditionals["topic"] = t.name
	t.loggerAdditionals["distribution"] = t.distributionType
	t.loggerAdditionals["load_balance_strategy"] = t.loadBalanceStrategy

	t.logger.Log("debug", "Initializing topic...", t.loggerAdditionals)

	t.incoming = newChannel()
	t.outgoing = make([]*channel, 0)

	// While it is good to use crypto/rand, it has good performance penalty as it is
	// cryptographically secure thing. We don't use cryptography here, so:
	// nolint:gosec
	t.randomer = rand.New(rand.NewSource(time.Now().UnixNano()))

	t.balancer = t.loadBalancerUnknown
	t.loadBalanceStrategy = pubsub.LoadBalancingStrategyUnknown

	// We should derive passed context to our local cancellable one for proper
	// shutdown handling.
	newCtx, cancelFunc := context.WithCancel(t.ctx)
	t.ctx = newCtx
	t.cancelFunc = cancelFunc

	return nil
}

// Sends message to distribution goroutine for processing.
func (t *topic) send(msg *pubsub.Message) bool {
	t.logger.Logf("debug", "Sending message: %+v", t.loggerAdditionals, msg)

	return t.incoming.channel.Send(msg)
}

// Starts worker that will distribute incoming messages to subscribers.
func (t *topic) start() {
	t.logger.Logf("debug", "Starting topic workers...", t.loggerAdditionals)

	go t.worker()
}

// Stops worker by sending end item to it.
func (t *topic) stop() {
	t.cancelFunc()
	t.workWaiter.Wait()
}

// Creates new outgoing channel effectively "subscribes" to topic.
func (t *topic) subscribe() *channel {
	t.logger.Logf("debug", "Subscribing to topic", t.loggerAdditionals)

	c := newChannel()

	t.outgoingMutex.Lock()
	defer t.outgoingMutex.Unlock()

	t.outgoing = append(t.outgoing, c)

	return c
}

// Worker that is launched in separate goroutine.
func (t *topic) worker() {
	t.workWaiter.Add(1)
	defer t.workWaiter.Done()

	t.logger.Log("debug", "Topic worker started.", t.loggerAdditionals)

	for {
		select {
		case msg := <-t.incoming.channel.GetChannel():
			switch t.distributionType {
			case pubsub.TypeDirect:
				t.balancer().Send(msg)
			case pubsub.TypeFanout:
				t.outgoingMutex.RLock()

				for _, subscriber := range t.outgoing {
					subscriber.Send(msg)
				}

				t.outgoingMutex.RUnlock()
			}
		case <-t.ctx.Done():
			t.logger.Log("debug", "Topic worker shutting down...", t.loggerAdditionals)

			// To ensure that nothing will be sent we should close everything
			// possible.
			t.incoming.channel.Close()

			// Also we should close all subscribers as nothing will be passed to them.
			t.outgoingMutex.RLock()
			defer t.outgoingMutex.RUnlock()
			for _, sub := range t.outgoing {
				sub.channel.Close()
			}

			return
		}
	}
}
