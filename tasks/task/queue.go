package task

import (
	"context"
	"sync"

	"gitlab.com/Contenido/framework/logging"
	"gitlab.com/Contenido/framework/pubsub"
)

// Internal queue representation.
type queue struct {
	name              string
	handler           QueueHandlerFunc
	workersCount      int
	logger            logging.Provider
	loggerAdditionals map[string]interface{}
	ctx               context.Context

	pubsubTopicName  string
	channels         []*pubsub.Channel
	queueStartWaiter sync.WaitGroup
}

// Actually starts queue workers and waits until they stops.
func (q *queue) start() []error {
	q.queueStartWaiter.Add(q.workersCount)

	var queueWaiter sync.WaitGroup

	errors := make([]error, 0)

	for i := 0; i < q.workersCount; i++ {
		queueWaiter.Add(1)

		go func(errs *[]error, workerNumber int) {
			q.logger.Logf("debug", "Starting worker #%d for queue '%s'...", q.loggerAdditionals, workerNumber, q.name)

			q.queueStartWaiter.Done()

			err := q.handler(q.ctx, workerNumber)
			if err != nil {
				*errs = append(*errs, err)
			}

			q.logger.Logf("debug", "Worker #%d for queue '%s' done", q.loggerAdditionals, workerNumber, q.name)

			queueWaiter.Done()
		}(&errors, i)
	}

	q.logger.Logf("debug", "Waiting for workers in queue '%s' to complete", q.loggerAdditionals, q.name)
	queueWaiter.Wait()
	q.logger.Logf("debug", "All workers for queue '%s' are done", q.loggerAdditionals, q.name)

	// As queue is stopped - clear channels slice.
	q.channels = nil

	return errors
}

// Waits until all workers are started.
func (q *queue) waitUntilStarted() {
	q.queueStartWaiter.Wait()
}
