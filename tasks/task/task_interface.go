package task

import (
	"context"
	"time"

	"gitlab.com/Contenido/framework/pubsub"
)

// QueueHandlerFunc is a queue handling function signature. Second parameter is a worker ID.
// This function will be launched in separate goroutine as many times as workers count
// requested by user. When requesting a channel for listening passed integer should also
// be passed to GetChannelForQueueWorker().
type QueueHandlerFunc func(context.Context, int) error

// Tasker is a generic task interface that should be accepted by any
// implementation of tasks worker and what every task implementation
// should support.
type Tasker interface {
	// CalculateFirstStartTime calculates next start time based on passed timestamp and timeout.
	CalculateFirstStartTime(hour, minute int, timeout time.Duration) time.Time
	// CronableExecuted sets next execution time for task. Used by task pool and
	// should not be called by hand normally.
	CronableExecuted()
	// GetChannelForQueueWorker returns incoming channel for designated queue and specific
	// worker number.
	GetChannelForQueueWorker(name string, workerNumber int) (chan *pubsub.Message, error)
	// GetID returns task ID.
	GetID() string
	// GetNextStartTime returns next start time for task if task is cronable. Otherwise
	// it will return 0001-01-01 00:00:00 (default time.Time value).
	GetNextStartTime() time.Time
	// IsAllowMultipleInstances returns boolean value that indicating if multiple
	// instances of this task is allowed to run.
	IsAllowMultipleInstances() bool
	// IsCompleted returns boolean value that indicating task completion status.
	IsCompleted() bool
	// IsCronable returns true if task is cronable.
	IsCronable() bool
	// IsRunning returns boolean value that indicating task running status.
	IsRunning() bool
	// RegisterQueueHandler registers queue handler. Should be called before task start.
	RegisterQueueHandler(name string, handler QueueHandlerFunc, workersCount int) error
	// SetContext sets context.Context for task.
	SetContext(context.Context)
	// SetCronableData sets next start time and timeout between starts.
	SetCronableData(time.Time, time.Duration)
	// SendMessage sends a message to another queue. Returns true if message was sent
	// and false if isn't.
	SendMessage(queueName string, msg *pubsub.Message) bool
	// SetAllowMultipleInstances allows multiple instances of same task to be launched.
	SetAllowMultipleInstances()
	// SetNotCronable removes possibly added next start time and timeout between starts.
	// Useful if you have to execute a task ASAP which by default is cronable (e.g. when
	// someone triggers it via communication).
	SetNotCronable()
	// SetID sets task ID for logging and identification purposes. Called automatically
	// by task worker when starting it.
	SetID(string)
	// ShouldStop returns boolean value indicating that we should stop executing
	// task. This is kind-of-workaround for code pieces that cannot use context.Context
	// for termination.
	ShouldStop() bool
	// Start starts task execution. If no context was set with SetContext() - default
	// context.Background() will be used.
	Start() error
	// StartWithContext starts task execution with passed context. This is an
	// alternative to SetContext() -> Start() calls.
	StartWithContext(context.Context) error
	// Stop stops task execution.
	Stop() error
}
