package task

import (
	"gitlab.com/Contenido/framework/errors"
	"gitlab.com/Contenido/framework/pubsub"
)

// GetChannelForQueueWorker returns incoming channel for designated queue and specific
// worker number.
func (t *Task) GetChannelForQueueWorker(name string, workerNumber int) (chan *pubsub.Message, error) {
	t.logger.Logf("debug", "Getting incoming channel for queue '%s' for worker #%d...", t.loggerAdditionals, name, workerNumber)

	t.queuesMutex.RLock()
	defer t.queuesMutex.RUnlock()

	q, ok := t.getQueue(name)
	if !ok {
		return nil, errors.ErrTasksQueueNotFound
	}

	if len(q.channels) < workerNumber {
		return nil, errors.ErrTasksWorkerNotSpecified
	}

	c := q.channels[workerNumber].GetChannel()
	if c == nil {
		return nil, errors.ErrTasksQueueChannelIsNil
	}

	return c, nil
}

// Returns true if at least 1 queue handler is registered.
func (t *Task) isQueuesSet() bool {
	t.queuesMutex.RLock()
	defer t.queuesMutex.RUnlock()

	return len(t.queues) > 0
}

// RegisterQueueHandler registers queue handler. Should be called before task start.
func (t *Task) RegisterQueueHandler(name string, handler QueueHandlerFunc, workersCount int) error {
	t.queuesMutex.Lock()
	defer t.queuesMutex.Unlock()

	t.logger.Logf("debug", "Registering queue handler '%s' with %d workers", t.loggerAdditionals, name, workersCount)

	q := &queue{
		name:              name,
		handler:           handler,
		workersCount:      workersCount,
		channels:          make([]*pubsub.Channel, 0, workersCount),
		logger:            t.logger,
		loggerAdditionals: t.loggerAdditionals,
	}

	_, registered := t.getQueue(name)
	if registered {
		return errors.ErrTasksQueueHandlerAlreadyRegistered
	}

	t.queues = append(t.queues, q)

	t.logger.Logf("debug", "Registered queue handler '%s' with %d workers", t.loggerAdditionals, name, workersCount)

	return nil
}

// SendMessage sends a message to another queue.
func (t *Task) SendMessage(queueName string, msg *pubsub.Message) bool {
	msgToSend := &pubsub.Message{
		Topic:   "internal/tasks/" + t.id + "/" + queueName,
		Message: msg.Message,
	}

	sent, err := t.pubsub.Send("simple", msgToSend)
	if err != nil {
		t.logger.Logf("error", "Failed to send message to queue '%s': %s", t.loggerAdditionals, err.Error())
	}

	return sent
}

// Starts queues on task starting.
func (t *Task) startQueues() error {
	t.queuesMutex.RLock()
	defer t.queuesMutex.RUnlock()

	t.queuesStarterWaiter.Add(len(t.queues))

	// First iteration - create channels.
	for _, q := range t.queues {
		if q.channels == nil {
			q.channels = make([]*pubsub.Channel, 0, q.workersCount)
		}

		t.logger.Logf("debug", "Preparing to start queue handler '%s'", t.loggerAdditionals, q.name)

		// We have context only when topic is started, that's why it set here.
		q.ctx = t.GetContext()

		// As we might know task ID only when starting - generate pubsub topic
		// name here.
		q.pubsubTopicName = "internal/tasks/" + t.id + "/" + q.name

		t.logger.Logf("debug", "Creating pubsub topic '%s' for queue '%s'", t.loggerAdditionals, q.pubsubTopicName, q.name)

		t.pubsub.CreateTopicWithContext(
			"simple",
			q.pubsubTopicName,
			pubsub.TypeDirect,
			pubsub.LoadBalancingStrategyLeastSent,
			t.GetContext(),
		)

		t.logger.Logf(
			"debug",
			"Creating %d subscriptions to topic '%s' for queue '%s'...",
			t.loggerAdditionals,
			q.workersCount,
			q.pubsubTopicName,
			q.name,
		)

		for i := 0; i < q.workersCount; i++ {
			t.logger.Logf("debug", "Creating subscription #%d for topic '%s' for queue '%s'...",
				t.loggerAdditionals,
				i,
				q.pubsubTopicName,
				q.name,
			)

			c, err := t.pubsub.Subscribe("simple", q.pubsubTopicName)
			if err != nil {
				return err
			}

			q.channels = append(q.channels, c)

			t.logger.Logf("debug", "Subscription #%d for queue '%s' is created and ready to use.", t.loggerAdditionals, i, q.name)
		}
	}

	// Second iterations - actually starts queues.
	for _, q := range t.queues {
		t.queuesWaiter.Add(1)

		go func(q *queue) {
			t.logger.Logf("debug", "Starting queue '%s'...", t.loggerAdditionals, q.name)

			t.queuesStarterWaiter.Done()

			errs := q.start()
			if len(errs) > 0 {
				t.logger.Logf("error", "Errors appeared when running queue '%s': %+v", t.loggerAdditionals, q.name, errs)
			}

			t.queuesWaiter.Done()

			// If one queue is done - we should stop all queues?
			t.cancel()
		}(q)
	}

	t.queuesStarterWaiter.Wait()
	t.queuesWaiter.Wait()

	return nil
}

func (t *Task) stopQueues() {
	t.queuesMutex.RLock()
	defer t.queuesMutex.RUnlock()

	for _, q := range t.queues {
		t.pubsub.StopTopic("simple", q.pubsubTopicName)
	}
}
