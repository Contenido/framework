package task

import (
	"context"
	"sync"
	"time"

	"gitlab.com/Contenido/framework/errors"
	"gitlab.com/Contenido/framework/logging"
	"gitlab.com/Contenido/framework/pubsub"
)

// Func is a function signature to use with task.
type Func func(context.Context) error

// Task describes generic task structure. Every other task should embed it to
// be used with task processing providers.
type Task struct {
	logger            logging.Provider
	loggerAdditionals map[string]interface{}

	pubsub *pubsub.Handler

	// Task ID.
	id string

	// Task context things.
	mainCtx context.Context // Main context which is obtained from framework.Application.
	ctx     context.Context // Derived from mainCtx on every task launch.
	cancel  context.CancelFunc

	// Internal state flags.
	launched bool // Launched state.

	// Stop indicator. Task handler should use t.ShouldStop()
	// to check whenever it should stop or not.
	shouldStop bool

	// Completion indicator.
	completed bool

	// Is multiple instances of same task are allowed to run?
	multipleInstancesAllowed bool

	// ..and it's mutexes.
	launchedMutex                 sync.RWMutex
	shouldStopMutex               sync.RWMutex
	completedMutex                sync.RWMutex
	multipleInstancesAllowedMutex sync.RWMutex

	// Cronable tasks related.
	queuedAt      time.Time     // Timestamp on which task should be first executed.
	timeout       time.Duration // Timeout that will be added after task execution.
	cronableMutex sync.RWMutex

	// Queues related fields.
	queues              []*queue // Known queues registered via RegisterQueueHandler
	queuesMutex         sync.RWMutex
	queuesWaiter        sync.WaitGroup // WaitGroup for waiting until all queues will be done.
	queuesStarterWaiter sync.WaitGroup // WaitGroup for waiting until all queues will start.
}

func (t *Task) getQueue(name string) (*queue, bool) {
	for _, v := range t.queues {
		if v != nil && v.name == name {
			return v, true
		}
	}
	return nil, false
}

// GetCancelFunc returns cancellation function. Should not be called manually!
func (t *Task) GetCancelFunc() context.CancelFunc {
	return t.cancel
}

// GetContext returns task's context.
func (t *Task) GetContext() context.Context {
	return t.ctx
}

// GetID returns task ID.
func (t *Task) GetID() string {
	return t.id
}

// InitializeTask initializes internal task things like logger.
func (t *Task) InitializeTask(logger logging.Provider, pubsub *pubsub.Handler) error {
	t.logger = logger
	t.pubsub = pubsub

	t.loggerAdditionals = make(map[string]interface{})

	return nil
}

// IsAllowMultipleInstances returns boolean value that indicating if multiple
// instances of this task is allowed to run.
func (t *Task) IsAllowMultipleInstances() bool {
	t.multipleInstancesAllowedMutex.RLock()
	defer t.multipleInstancesAllowedMutex.RUnlock()

	return t.multipleInstancesAllowed
}

// IsCompleted returns boolean value indicating that task is completed
// (e.g. was launched and eventually completed).
func (t *Task) IsCompleted() bool {
	t.completedMutex.RLock()
	defer t.completedMutex.RUnlock()

	return t.completed
}

// IsRunning returns boolean value indicating that task is running.
func (t *Task) IsRunning() bool {
	t.launchedMutex.RLock()
	defer t.launchedMutex.RUnlock()

	return t.launched
}

// SetAllowMultipleInstances allows multiple instances of same task to be launched.
func (t *Task) SetAllowMultipleInstances() {
	t.multipleInstancesAllowedMutex.Lock()
	defer t.multipleInstancesAllowedMutex.Unlock()

	t.multipleInstancesAllowed = true
}

// SetContext sets task context if needed.
func (t *Task) SetContext(ctx context.Context) {
	t.mainCtx = ctx
}

// Sets "completed" flag.
func (t *Task) setCompleted() {
	t.completedMutex.Lock()
	defer t.completedMutex.Unlock()
	t.completed = true
}

// SetID sets task ID for logging purposes.
func (t *Task) SetID(id string) {
	t.id = id

	t.loggerAdditionals["task_id"] = t.id
}

// SetShouldStop sets should stop flag. Can be used by task kind to indicate that we
// should stop due to some reason, e.g. context cancellation or error.
func (t *Task) SetShouldStop() {
	t.shouldStopMutex.Lock()
	defer t.shouldStopMutex.Unlock()

	t.shouldStop = true
}

// ShouldStop returns boolean value indicating that we should stop doing things.
// This function should be used in task itself to check if it should stop
// prematurely or not.
func (t *Task) ShouldStop() bool {
	t.shouldStopMutex.RLock()
	defer t.shouldStopMutex.RUnlock()

	return t.shouldStop
}

// Start starts task execution. It will create own context and call StartWithContext
// after that so task will be stoppable. Use this function only if task should be
// run in background without any interaction, otherwise use StartWithContext().
func (t *Task) Start() error {
	if t.mainCtx != nil {
		return t.StartWithContext(t.ctx)
	}

	return t.StartWithContext(context.Background())
}

// StartWithContext starts task execution with context. It will start task execution
// in another goroutine. Use this function only if task should end when context
// is cancelled on higher level (e.g. when remote user stops request), otherwise
// use Start().
func (t *Task) StartWithContext(ctx context.Context) error {
	t.logger.Log("debug", "Starting task...", t.loggerAdditionals)

	if !t.isQueuesSet() {
		return errors.ErrTaskQueuesNotSet
	}

	t.ctx, t.cancel = context.WithCancel(ctx)

	// Separate goroutine that checks cancellation and sets internal flag that
	// indicates neccessity of stopping for task.
	go func() {
		t.queuesStarterWaiter.Wait()

		t.logger.Log("debug", "All queues started, listening to context interruption...", nil)

		<-t.ctx.Done()

		t.logger.Log("debug", "Context interruption received, stopping task...", t.loggerAdditionals)

		t.SetShouldStop()

		t.stopQueues()
	}()

	t.startQueues()

	// Stop everything we've created when queues are done.
	t.logger.Log("debug", "All queues done, cancelling everything underlying and remaining...", t.loggerAdditionals)
	t.cancel()

	t.logger.Log("debug", "Marking task as completed.", t.loggerAdditionals)
	t.setCompleted()

	return nil
}

// Stop stops task execution.
func (t *Task) Stop() error {
	t.cancel()

	// Wait for task completion.
	for {
		if t.IsCompleted() {
			break
		}

		time.Sleep(time.Millisecond * 100)
	}

	return nil
}

// WaitUntilQueueStarted blocks execution until all workers for passed queue will be
// started.
func (t *Task) WaitUntilQueueStarted(queueName string) {
	t.queuesMutex.RLock()
	defer t.queuesMutex.RUnlock()

	q, found := t.getQueue(queueName)
	if !found {
		t.logger.Logf("error", "Queue '%s' wasn't found, can't wait until it's fully started!", t.loggerAdditionals, queueName)

		return
	}

	q.waitUntilStarted()
}

// WaitUntilStarted block execution until all workers of all queues will be started.
func (t *Task) WaitUntilStarted() {
	t.queuesStarterWaiter.Wait()
}
