package task

import (
	"time"
)

// CalculateFirstStartTime calculates next start time in UTC based on passed timestamp
// and timeout. Warning - passed hour and minute should also be in UTC!
func (t *Task) CalculateFirstStartTime(hour, minute int, timeout time.Duration) time.Time {
	curTime := time.Now().UTC()
	firstLaunch := time.Date(curTime.Year(), curTime.Month(), curTime.Day(), hour, minute, 0, 0, time.UTC)

	return firstLaunch.Add(timeout)
}

// CronableExecuted adds timeout to queued at timestamp. Should not be called manually,
// used by tasks pools.
func (t *Task) CronableExecuted() {
	t.cronableMutex.Lock()
	defer t.cronableMutex.Unlock()

	t.queuedAt = t.queuedAt.Add(t.timeout)
}

// GetNextStartTime returns next task start time for cronable tasks.
func (t *Task) GetNextStartTime() time.Time {
	t.cronableMutex.RLock()
	defer t.cronableMutex.RUnlock()

	return t.queuedAt
}

// IsCronable returns true if task is queued to be executed at specific time.
func (t *Task) IsCronable() bool {
	t.cronableMutex.RLock()
	defer t.cronableMutex.RUnlock()

	return t.queuedAt.Year() >= time.Now().UTC().Year()
}

// SetCronable sets first execution time and a timeout which will be added to that
// time after task is executed.
func (t *Task) SetCronableData(firstExecution time.Time, timeout time.Duration) {
	t.cronableMutex.Lock()
	defer t.cronableMutex.Unlock()

	t.queuedAt = firstExecution
	t.timeout = timeout
}

// SetNotCronable sets task to be not cronable. Useful when by default task should
// be executed at specific time (and SetCronable() called at task creation), but in that
// specific case task should not be queued and executed immediately.
func (t *Task) SetNotCronable() {
	t.cronableMutex.Lock()
	defer t.cronableMutex.Unlock()

	t.queuedAt = time.Date(0, time.January, 1, 0, 0, 0, 0, time.UTC)
}
