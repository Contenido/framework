package pool

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"gitlab.com/Contenido/framework/communication/request"
	"gitlab.com/Contenido/framework/tasks/task"
)

func (p *Pool) getListOfRunningTasks(reqctx request.Context) error {
	p.runningTasksMutex.RLock()
	defer p.runningTasksMutex.RUnlock()

	taskUUIDs := make([]string, 0, len(p.runningTasks))

	for taskUUID := range p.runningTasks {
		taskUUIDs = append(taskUUIDs, taskUUID)
	}

	return reqctx.WriteJSON(http.StatusOK, taskUUIDs)
}

func (p *Pool) stopTasks(reqctx request.Context) error {
	tasksToStopRaw, err := ioutil.ReadAll(reqctx.Body())
	if err != nil {
		return reqctx.WriteString(http.StatusInternalServerError, err.Error())
	}

	var tasksToStop []string

	err = json.Unmarshal(tasksToStopRaw, &tasksToStop)
	if err != nil {
		return reqctx.WriteString(http.StatusBadRequest, err.Error())
	}

	p.runningTasksMutex.RLock()
	for _, taskUUID := range tasksToStop {
		t, found := p.runningTasks[taskUUID]
		if found {
			go func(t task.Tasker) {
				if err := t.Stop(); err != nil {
					p.logger.Logf(
						"error",
						"Error appeared when trying to stop task %s: %s",
						defaultLoggerAdditionals,
						taskUUID, err.Error(),
					)
				}
			}(t)
		}
	}
	p.runningTasksMutex.RUnlock()

	return reqctx.WriteString(http.StatusOK, "OK")
}
