package pool

import (
	"fmt"
	"runtime"
	"sync"
	"time"

	"github.com/gofrs/uuid"

	"gitlab.com/Contenido/framework"
	"gitlab.com/Contenido/framework/communication"
	"gitlab.com/Contenido/framework/errors"
	"gitlab.com/Contenido/framework/internal/provider"
	"gitlab.com/Contenido/framework/logging"
	"gitlab.com/Contenido/framework/tasks/task"
)

var defaultLoggerAdditionals = map[string]interface{}{
	"type": "core",
	"name": "tasks pool",
}

// Pool is a tasks controlling pool. By default it will allow
// to run as much tasks as cores count but you can override it with
// Pool.SetMaximumRunningTasksCount() call.
// Pool will not start any goroutine until task will arrive.
type Pool struct {
	logger       logging.Provider
	runningTasks map[string]task.Tasker // Currently running tasks. Keys are UUIDs as strings.
	app          *framework.Application
	provider.BaseProvider
	enqueuedTasks          []task.Tasker // Enqueued with Pool.Enqueue() tasks. They're completely asynchronous.
	highPriorityTasks      []task.Tasker // Queued with Pool.Run() tasks. They might be synchronous!
	maximumRunning         int           // Maximum running tasks count.
	maximumRunningMutex    sync.RWMutex
	runningTasksMutex      sync.RWMutex
	enqueuedTasksMutex     sync.RWMutex
	highPriorityTasksMutex sync.RWMutex
	shouldShutdownMutex    sync.RWMutex
	shouldShutdown         bool // Shutdown flag.
}

// NewProvider creates new tasks pool.
func NewProvider(app *framework.Application) *Pool {
	// As this is a provider instantiation there is no actual possibility to fill all
	// fields (they'll be filled with Initialize() call), so:
	// nolint:exhaustivestruct
	p := &Pool{
		app:            app,
		maximumRunning: runtime.NumCPU(),
	}

	return p
}

// Clears running queue from completed tasks.
func (p *Pool) clearCompletedTasks() {
	var tasksToDelete []string

	// Every task, when completed, sets internal "completed" flag but not immediately
	// removed from running tasks mapping. So we should check if we have completed
	// tasks to remove from running tasks mapping.
	p.runningTasksMutex.RLock()
	for _, t := range p.runningTasks {
		if t.IsCompleted() {
			tasksToDelete = append(tasksToDelete, t.GetID())
		}
	}
	p.runningTasksMutex.RUnlock()

	// If we have something to delete - do it. This will also unblock possibility
	// to start task again on next worker iteration.
	if len(tasksToDelete) > 0 {
		p.logger.Logf(
			"debug",
			"Got %d tasks to delete from running tasks mapping",
			defaultLoggerAdditionals,
			len(tasksToDelete),
		)

		p.runningTasksMutex.Lock()

		for _, taskUUID := range tasksToDelete {
			p.logger.Logf("debug", "Deleting task %s...", defaultLoggerAdditionals, taskUUID)
			delete(p.runningTasks, taskUUID)
		}

		p.runningTasksMutex.Unlock()
	}
}

// Creates empty enqueued tasks queue.
func (p *Pool) createEnqueuedTasksQueue() {
	if p.app.Configuration.Config.Framework.TasksWorker.Logger.LogQueueRecreations {
		p.logger.Log("debug", "(Re)Creating normal priority tasks queue", defaultLoggerAdditionals)
	}

	p.maximumRunningMutex.RLock()
	defer p.maximumRunningMutex.RUnlock()

	p.enqueuedTasksMutex.Lock()
	defer p.enqueuedTasksMutex.Unlock()

	p.enqueuedTasks = make([]task.Tasker, 0, p.maximumRunning)
}

// Creates empty high priority tasks queue.
func (p *Pool) createHighPriorityTasksQueue() {
	if p.app.Configuration.Config.Framework.TasksWorker.Logger.LogQueueRecreations {
		p.logger.Log("debug", "(Re)Creating high priority tasks queue", defaultLoggerAdditionals)
	}

	p.maximumRunningMutex.RLock()
	defer p.maximumRunningMutex.RUnlock()

	p.highPriorityTasksMutex.Lock()
	defer p.highPriorityTasksMutex.Unlock()

	p.highPriorityTasks = make([]task.Tasker, 0, p.maximumRunning)
}

// Enqueue adds task to queue for execution. If you want to execute task
// asynchronously - you should enqueue it. For synchronous execution use Run().
// Enqueue, as Run(), does not check if passed task is unique, so if you've
// added two similar tasks - they'll be executed. Function returns task UUID
// for possible later usage and UUID creation error.
func (p *Pool) Enqueue(t task.Tasker) (string, error) {
	taskUUID := t.GetID()

	var taskUUIDWasSet bool

	if taskUUID == "" {
		taskUUIDRaw, err := uuid.NewV4()
		if err != nil {
			return "", fmt.Errorf("pool tasks provider: enqueue: task UUID generation: %w", err)
		}

		taskUUID = taskUUIDRaw.String()

		t.SetID(taskUUID)
	} else {
		taskUUIDWasSet = true
	}

	if !taskUUIDWasSet {
		p.logger.Logf("debug", "Queing task %s", defaultLoggerAdditionals, taskUUID)
	}

	p.enqueuedTasksMutex.Lock()
	p.enqueuedTasks = append(p.enqueuedTasks, t)
	p.enqueuedTasksMutex.Unlock()

	if t.IsCronable() {
		if !taskUUIDWasSet {
			p.logger.Logf(
				"debug",
				"Task %s is queued for starting at %s",
				defaultLoggerAdditionals,
				taskUUID,
				t.GetNextStartTime().String(),
			)
		}
	} else {
		p.logger.Logf("debug", "Task %s queued for starting ASAP", defaultLoggerAdditionals, taskUUID)
	}

	return taskUUID, nil
}

// Gets a slice of high priority tasks to run.
func (p *Pool) getHighPriorityTasks(maximumCount int) []task.Tasker {
	if p.app.Configuration.Config.Framework.TasksWorker.Logger.LogWorkerIterations {
		p.logger.Log("debug", "Getting high priority tasks...", defaultLoggerAdditionals)
	}

	var (
		tasksToRun     []task.Tasker
		shouldRecreate bool
	)

	p.highPriorityTasksMutex.Lock()

	if len(p.highPriorityTasks) > maximumCount {
		tasksToRun = p.highPriorityTasks[:maximumCount]
		p.highPriorityTasks = p.highPriorityTasks[maximumCount:]
	} else {
		tasksToRun = append(tasksToRun, p.highPriorityTasks...)
		shouldRecreate = true
	}

	p.highPriorityTasksMutex.Unlock()

	if shouldRecreate {
		p.createHighPriorityTasksQueue()
	}

	if p.app.Configuration.Config.Framework.TasksWorker.Logger.LogWorkerIterations {
		p.logger.Logf("debug", "Got %d high priority tasks to run", defaultLoggerAdditionals, len(tasksToRun))
	}

	return tasksToRun
}

// Gets a slice of normal tasks to run.
func (p *Pool) getNormalTasks(maximumCount int) []task.Tasker {
	if p.app.Configuration.Config.Framework.TasksWorker.Logger.LogWorkerIterations {
		p.logger.Log("debug", "Getting normal priority tasks...", defaultLoggerAdditionals)
	}

	var (
		tasksToRun     []task.Tasker
		shouldRecreate bool
	)

	p.enqueuedTasksMutex.Lock()

	if len(p.enqueuedTasks) > maximumCount {
		tasksToRun = p.enqueuedTasks[:maximumCount]
		p.enqueuedTasks = p.enqueuedTasks[maximumCount:]
	} else {
		tasksToRun = append(tasksToRun, p.enqueuedTasks...)
		shouldRecreate = true
	}

	p.enqueuedTasksMutex.Unlock()

	if shouldRecreate {
		p.createEnqueuedTasksQueue()
	}

	if p.app.Configuration.Config.Framework.TasksWorker.Logger.LogWorkerIterations {
		p.logger.Logf("debug", "Got %d normal priority tasks to run", defaultLoggerAdditionals, len(tasksToRun))
	}

	return tasksToRun
}

// Returns a list of tasks to start, if possible.
func (p *Pool) getTasksToRun() []task.Tasker {
	tasksToRun := make([]task.Tasker, 0)

	p.runningTasksMutex.RLock()
	currentlyRunning := len(p.runningTasks)
	p.runningTasksMutex.RUnlock()

	p.maximumRunningMutex.RLock()
	maximumRunning := p.maximumRunning
	p.maximumRunningMutex.RUnlock()

	canBeRun := maximumRunning - currentlyRunning
	// Do not do anything if we're at maximum.
	if canBeRun < 1 {
		if p.app.Configuration.Config.Framework.TasksWorker.Logger.LogWorkerIterations {
			p.logger.Log("debug", "Nothing can be started, we're at maximum running tasks count", defaultLoggerAdditionals)
		}

		return tasksToRun
	}

	tasksToRun = append(tasksToRun, p.getHighPriorityTasks(canBeRun)...)

	if len(tasksToRun) < canBeRun {
		tasksToRun = append(tasksToRun, p.getNormalTasks(canBeRun-len(tasksToRun))...)
	}

	return tasksToRun
}

// Initialize performs initialization actions.
func (p *Pool) Initialize() error {
	if p.runningTasks == nil {
		p.runningTasks = make(map[string]task.Tasker)
	}

	logger, err := p.app.Logger.GetProvider(logging.DefaultLoggingProvider)
	if err != nil {
		return fmt.Errorf("pool tasks provider: get logging provider: %w", err)
	}

	p.logger = logger

	p.createEnqueuedTasksQueue()
	p.createHighPriorityTasksQueue()

	p.RegisterStartFunc(p.registerHTTPEndpoints)
	p.RegisterStartFunc(p.start)
	p.RegisterStopFunc(p.stop)

	return nil
}

// Registers general HTTP endpoints for working with tasks using internal HTTP server.
func (p *Pool) registerHTTPEndpoints() error {
	// Inject own API routes into internal HTTP server is initialized.
	// ToDo:pztrn - rework to use contants!
	internalHTTPServerRaw, err := p.app.Communication.GetProvider(framework.InternalHTTPServer)
	if err != nil {
		return fmt.Errorf("registering tasks pool HTTP API: %w", err)
	}

	internalHTTPServer, ok := internalHTTPServerRaw.(communication.Provider)
	if !ok {
		return errors.ErrInvalidProvider
	}

	if err := internalHTTPServer.RegisterHandler("GET", "/api/v1/tasks/running", p.getListOfRunningTasks); err != nil {
		p.logger.Logf(
			"error",
			"Failed to register HTTP handler for task starting routine: %s",
			defaultLoggerAdditionals,
			err.Error(),
		)
	}

	if err := internalHTTPServer.RegisterHandler("POST", "/api/v1/tasks/stop", p.stopTasks); err != nil {
		p.logger.Logf(
			"error",
			"Failed to register HTTP handler for task stopping routine: %s",
			defaultLoggerAdditionals,
			err.Error(),
		)
	}

	return nil
}

// Run runs task immediately (if possible) or wait until in can be run. This
// is a blocking call and will return only when task is over (or if we're
// unable to create UUID for it).
func (p *Pool) Run(task task.Tasker) error {
	taskUUID, err := uuid.NewV4()
	if err != nil {
		return fmt.Errorf("pool tasks provider: run task: generate task UUID: %w", err)
	}

	p.logger.Logf(
		"debug",
		"Queing task %s as high-priority (will be launched ASAP)",
		defaultLoggerAdditionals,
		taskUUID.String(),
	)

	p.highPriorityTasksMutex.Lock()
	p.highPriorityTasks = append(p.highPriorityTasks, task)
	p.highPriorityTasksMutex.Unlock()

	p.logger.Logf("debug", "Task %s queued as high-priority", defaultLoggerAdditionals, taskUUID.String())

	task.SetID(taskUUID.String())

	for {
		if task.IsCompleted() {
			break
		}

		// This helps not to eat 100% of CPU (or one full core) while we're
		// waiting for task.IsCompleted() became true.
		time.Sleep(time.Millisecond * 500)
	}

	return nil
}

// SetConfig sets provider's connection configuration.
func (p *Pool) SetConfig(config interface{}) error {
	return nil
}

// SetMaximumRunningTasksCount sets maximum amount of tasks that can run
// simultaneously. By default pool will allow as much simultaneous tasks to be
// run as cores count. This function is safe to be used in runtime.
func (p *Pool) SetMaximumRunningTasksCount(count int) {
	p.maximumRunningMutex.Lock()
	defer p.maximumRunningMutex.Unlock()

	p.maximumRunning = count
}

// Starts pool worker.
func (p *Pool) start() error {
	go p.worker()

	return nil
}

// Stops pool and all running tasks.
func (p *Pool) stop() error {
	p.logger.Log("debug", "Stopping tasks worker pool...", defaultLoggerAdditionals)

	p.shouldShutdownMutex.Lock()
	p.shouldShutdown = true
	p.shouldShutdownMutex.Unlock()

	p.runningTasksMutex.Lock()
	defer p.runningTasksMutex.Unlock()

	for taskUUID, task := range p.runningTasks {
		p.logger.Logf("debug", "Stopping task %s...", defaultLoggerAdditionals, taskUUID)

		if err := task.Stop(); err != nil {
			p.logger.Logf(
				"error",
				"Error appeared when trying to stop task %s: %s",
				defaultLoggerAdditionals,
				taskUUID,
				err.Error(),
			)
		}
	}

	return nil
}

// Pool worker.
// ToDo: remove nolint and refactor this function.
// nolint:gocognit,cyclop
func (p *Pool) worker() {
	p.logger.Log("debug", "Starting tasks pool worker...", defaultLoggerAdditionals)

	// ToDo: confgurable?
	ticker := time.NewTicker(time.Second)

	for range ticker.C {
		if p.app.Configuration.Config.Framework.TasksWorker.Logger.LogWorkerIterations {
			p.logger.Log("debug", "Tasks pool worker iteration started", defaultLoggerAdditionals)
		}

		// Check if we should stop working.
		p.shouldShutdownMutex.RLock()
		shouldShutdown := p.shouldShutdown
		p.shouldShutdownMutex.RUnlock()

		if shouldShutdown {
			p.logger.Logf("debug", "Tasks pool worker should shutdown, doing it...", defaultLoggerAdditionals)

			break
		}

		// Check if some of currently running tasks already completed it's work.
		p.clearCompletedTasks()

		// Check if we can launch additional tasks.
		tasksToRun := p.getTasksToRun()
		if len(tasksToRun) == 0 {
			if p.app.Configuration.Config.Framework.TasksWorker.Logger.LogWorkerIterations {
				p.logger.Log("debug", "No tasks to run, skipping iteration", defaultLoggerAdditionals)
			}

			continue
		}

		if p.app.Configuration.Config.Framework.TasksWorker.Logger.LogWorkerIterations {
			p.logger.Logf("debug", "Got %d tasks to run", defaultLoggerAdditionals, len(tasksToRun))
		}

		for _, t := range tasksToRun {
			if p.app.Configuration.Config.Framework.TasksWorker.Logger.LogWorkerIterations {
				p.logger.Logf("debug", "Checking if task %s should be started now...", defaultLoggerAdditionals, t.GetID())
			}

			// Check if task is cronnable and should (or should not) be run.
			var shouldBeStarted bool

			// If task is cronable and current time after time task was queued at - task
			// should be executed.
			if t.IsCronable() && time.Now().UTC().After(t.GetNextStartTime()) {
				p.logger.Logf(
					"debug",
					"Task %s is cronable and timeout has passed, task will be started",
					defaultLoggerAdditionals,
					t.GetID(),
				)

				shouldBeStarted = true
			} else if !t.IsCronable() {
				p.logger.Logf("debug", "Task %s isn't cronable so it should be started ASAP", defaultLoggerAdditionals, t.GetID())
				// If task isn't cronable - it should also be executed ASAP.
				shouldBeStarted = true
			}

			if shouldBeStarted {
				go func(taskToStart task.Tasker) {
					if err := taskToStart.Start(); err != nil {
						p.logger.Logf("error", "Task '%s' failed: %s", defaultLoggerAdditionals, err.Error())
					}

					p.logger.Logf("debug", "Removing task %s from list of running tasks", defaultLoggerAdditionals, taskToStart.GetID())

					p.runningTasksMutex.Lock()
					delete(p.runningTasks, taskToStart.GetID())
					p.runningTasksMutex.Unlock()

					// Re-queue cronnable task.
					if taskToStart.IsCronable() {
						taskToStart.CronableExecuted()
						_, _ = p.Enqueue(taskToStart)
					}
				}(t)

				p.runningTasksMutex.Lock()
				p.runningTasks[t.GetID()] = t
				p.runningTasksMutex.Unlock()
			} else {
				if p.app.Configuration.Config.Framework.TasksWorker.Logger.LogWorkerIterations {
					p.logger.Logf("debug", "Task %s won't be started now", defaultLoggerAdditionals, t.GetID())
				}

				_, _ = p.Enqueue(t)
			}
		}
	}
}
