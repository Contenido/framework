package tasks

import (
	"fmt"

	"gitlab.com/Contenido/framework/errors"
	"gitlab.com/Contenido/framework/internal/handler"
	"gitlab.com/Contenido/framework/tasks/task"
)

// Handler handles all tasks things application will use.
// Tasks processing providers should be registered before application's
// business logic initialization in order to be properly used.
type Handler struct {
	handler.BaseHandler
}

// Enqueue adds task to queue for execution. If you want to execute task
// asynchronously - you should enqueue it. For synchronous execution use Run().
// Enqueue, as Run(), does not check if passed task is unique, so if you've
// added two similar tasks - they'll be executed. Function returns task UUID
// for possible later usage and UUID creation error.
func (h *Handler) Enqueue(providerName string, t task.Tasker) (string, error) {
	provider, err := h.GetProvider(providerName)
	if err != nil {
		return "", err
	}

	return provider.Enqueue(t)
}

// GetProvider returns requested provider.
func (h *Handler) GetProvider(providerName string) (Provider, error) {
	providerRaw, err := h.GetRawProvider(providerName)
	if err != nil {
		return nil, fmt.Errorf("tasks provider: get provider: %w", err)
	}

	provider, ok := providerRaw.(Provider)
	if !ok {
		return nil, errors.ErrInvalidProvider
	}

	return provider, nil
}

// Initialize initializes handler's internal state.
func (h *Handler) Initialize() {}

// Run runs task in blocking mode.
func (h *Handler) Run(providerName string, t task.Tasker) error {
	provider, err := h.GetProvider(providerName)
	if err != nil {
		return err
	}

	return provider.Run(t)
}
