package tasks

import (
	"gitlab.com/Contenido/framework/internal/provider"
	"gitlab.com/Contenido/framework/tasks/task"
)

type Provider interface {
	provider.BaseInterface

	// Enqueue adds task to queue for execution. If you want to execute task
	// asynchronously - you should enqueue it. For synchronous execution use Run().
	// Enqueue, as Run(), does not check if passed task is unique, so if you've
	// added two similar tasks - they'll be executed. Function returns task UUID
	// for possible later usage and UUID creation error.
	Enqueue(task.Tasker) (string, error)
	// Run runs task immediately (if possible) or wait until in can be run. This
	// is a blocking call and will return only when task is over (or if we're
	// unable to create UUID for it).
	Run(task.Tasker) error
	// SetMaximumRunningTasksCount sets maximum amount of tasks that can run
	// simultaneously. By default pool will allow as much simultaneous tasks to be
	// run as cores count. This function is safe to be used in runtime.
	SetMaximumRunningTasksCount(int)
}
