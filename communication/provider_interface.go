package communication

import (
	"gitlab.com/Contenido/framework/communication/request"
	"gitlab.com/Contenido/framework/communication/response"
	"gitlab.com/Contenido/framework/internal/provider"
)

// Provider is an interface for communication channel provider.
type Provider interface {
	provider.BaseInterface

	// IsClient returns true if communication provider is able to do
	// client-server communication.
	IsClient() bool
	// IsServer returns true if communication provider is able to serving things.
	IsServer() bool
	// RegisterHandler registers handler for communication channel provider. Some
	// providers might ignore "method" parameter (e.g. message queues).
	RegisterHandler(method string, path string, handler HandlerFunc) error
	// Request sends request to remote thing in synchronous manner and returns
	// either a response or error.
	Request(request *request.Request) (*response.Response, error)
	// Send sends request to remote thing in synchronous manner and does not
	// return anything but protocol error.
	Send(request *request.Request) error
	// SendAsync sends request to remote thing in asynchronous manner. It
	// won't return any response or protocol errors, only internal for framework.
	SendAsync(request *request.Request) error
	// SendAsyncRaw sends request to remote thing in asynchronous manner. It
	// won't return any response or protocol errors, only internal for framework.
	// That method sends request's Data field as message and puts Headers into provider's headers,
	// if applicable, all other fields are ignored.
	SendAsyncRaw(request *request.Request) error
	// SetHeaderDelimiter sets delimiter var for separating same key headers if array sending is impossible.
	SetHeaderDelimiter(delimiter string)
}

// HandlerFunc is a function signature for communications handler.
type HandlerFunc func(request.Context) error
