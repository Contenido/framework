package httpecho

import "errors"

// ErrConfigurationIsNotForEcho appears when passed configuration is
// not for Echo HTTP server provider.
var ErrConfigurationIsNotForEcho = errors.New("passed configuration is not for Echo HTTP server provider")
