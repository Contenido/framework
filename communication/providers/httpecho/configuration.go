package httpecho

import (
	"net"

	"gitlab.com/Contenido/framework/errors"
)

// Configuration is a structure that describes Echo HTTP server
// configuration.
type Configuration struct {
	// Address is an address on which HTTP server will listen.
	Address string `json:"address"`
	// UseDefaultLoggingMiddleware indicates that user want to use
	// default logger.
	UseDefaultLoggingMiddleware bool `json:"use_default_logging_middleware"`
	// WaitForSeconds is a seconds count for which we will wait for
	// HTTP server to be up and ready to process requests.
	WaitForSeconds int `json:"wait_for_seconds"`
}

// Validate validates configuration. Called by provider itself when setting it
// with SetConfig(), no need to call it manually!
func (c *Configuration) Validate() error {
	// Checking configuration for values.
	// Listening address should not be empty string.
	if c.Address == "" {
		return errors.ErrNoHTTPServerListeningAddressDefined
	}

	// Listening address should be a "host:port" thing.
	if _, _, err := net.SplitHostPort(c.Address); err != nil {
		return errors.ErrInvalidHTTPServerListeningAddress
	}

	// WaitForSeconds should be positive and greater than zero.
	if c.WaitForSeconds < 1 {
		return errors.ErrInvalidHTTPServerWaitForSeconds
	}

	return nil
}
