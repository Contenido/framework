package httpecho

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/labstack/echo-contrib/prometheus"
	"github.com/labstack/echo/v4"
	"gitlab.com/Contenido/framework"
	"gitlab.com/Contenido/framework/communication"
	"gitlab.com/Contenido/framework/communication/request"
	"gitlab.com/Contenido/framework/communication/response"
	"gitlab.com/Contenido/framework/errors"
	"gitlab.com/Contenido/framework/internal/provider"
	"gitlab.com/Contenido/framework/logging"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	zl "github.com/rs/zerolog"
)

// Provider is a provider for HTTP communication channel using
// Labstack's Echo web framework.
type Provider struct {
	provider.BaseProvider

	app    *framework.Application
	cfg    *Configuration
	server *echo.Echo
	name   string
	logger zl.Logger
}

// NewProvider creates new HTTP server provider using echo framework.
func NewProvider(app *framework.Application, name string) *Provider {
	// As this is a provider instantiation there is no actual possibility to fill all
	// fields (they'll be filled with Initialize() call), so:
	// nolint:exhaustivestruct
	return &Provider{
		app:  app,
		name: name,
	}
}

// Default route handler will be called if no handler was registered for
// path.
func (htp *Provider) defaultRouteHandler(ec echo.Context) error {
	_ = ec.String(http.StatusBadRequest, "Unknown path.")

	return nil
}

// Initialize performs preliminary initialization actions like internal
// structures initialization. Should not be called manually.
func (htp *Provider) Initialize() error {
	logProvider, err := htp.app.Logger.GetProvider(logging.DefaultLoggingProvider)
	if err != nil {
		return fmt.Errorf("httpecho: %w", err)
	}

	logger, err := logProvider.GetLogger("http server", map[string]string{"name": htp.name})
	if err != nil {
		return fmt.Errorf("httpecho: %w", err)
	}

	htp.logger = logger.(zl.Logger)

	htp.logger.Info().Msg("Initializing provider...")

	htp.server = echo.New()
	htp.server.DisableHTTP2 = true
	htp.server.HideBanner = true
	htp.server.HidePort = true

	htp.server.Any("/*", htp.defaultRouteHandler)
	htp.server.GET("/_internal/waitForOnline", htp.waitForHTTPServerToBeUpHandler)

	htp.RegisterStartFunc(htp.startServer)
	htp.RegisterStopFunc(htp.stopServer)

	// THIS IS A TEMPORARY HACK UNTIL METRICS SUBSYSTEM WELL BE DONE IN
	// FRAMEWORK. Ye, usually "everything temporary became new permanent",
	// but this isn't that case.
	if htp.name == framework.InternalHTTPServer {
		htp.server.GET("/metrics", echo.WrapHandler(promhttp.Handler()))
	}

	p := prometheus.NewPrometheus("echo_"+htp.name, nil)
	p.Use(htp.server)
	// END OF TEMPORARY HACK.

	return nil
}

// IsClient always returns false as this is HTTP *server* provider.
func (htp *Provider) IsClient() bool { return false }

// IsClient always returns true as this is HTTP *server* provider.
func (htp *Provider) IsServer() bool { return true }

// RegisterHandler registers handler for communication channel.
func (htp *Provider) RegisterHandler(method string, path string, handler communication.HandlerFunc) error {
	// Simple wrapper around our handler function.
	h := func(ec echo.Context) error {
		ctx := &request.DefaultContext{}
		ctx.SetBody(ec.Request().Body)
		ctx.SetHeaders(request.NewHeadersFromHTTP(ec.Request().Header))
		ctx.SetContext(ec.Request().Context())
		ctx.SetProviderContext(ec)
		ctx.SetFrom(ec.RealIP())
		ctx.SetPath(ec.Path())
		ctx.SetProtocol("http")
		ctx.SetWriteJSONFunc(func(code int, data interface{}, additionals ...interface{}) error {
			return ec.JSON(code, data)
		})
		ctx.SetWriteStringFunc(func(code int, data interface{}, additionals ...interface{}) error {
			return ec.String(code, data.(string))
		})

		return handler(ctx)
	}

	// Figure out method-related function to use for handler registration.
	var echoFunc func(string, echo.HandlerFunc, ...echo.MiddlewareFunc) *echo.Route

	switch method {
	case http.MethodConnect:
		echoFunc = htp.server.CONNECT
	case http.MethodDelete:
		echoFunc = htp.server.DELETE
	case http.MethodGet:
		echoFunc = htp.server.GET
	case http.MethodHead:
		echoFunc = htp.server.HEAD
	case http.MethodOptions:
		echoFunc = htp.server.OPTIONS
	case http.MethodPatch:
		echoFunc = htp.server.PATCH
	case http.MethodPost:
		echoFunc = htp.server.POST
	case http.MethodPut:
		echoFunc = htp.server.PUT
	case http.MethodTrace:
		echoFunc = htp.server.TRACE
	default:
		return errors.ErrUnknownHTTPMethod
	}

	// Register our wrapped handler.
	echoFunc(path, h)

	return nil
}

// Request sends request to remote thing in synchronous manner and returns
// either a response or error. As this is server-only provider this
// function does nothing.
func (htp *Provider) Request(request *request.Request) (*response.Response, error) { return nil, nil }

// Send sends request to remote thing. As this is server-only provider this
// function does nothing.
func (htp *Provider) Send(request *request.Request) error { return nil }

// SendAsync sends request to remote thing in asynchronous manner. It
// won't return any response or protocol errors, only internal for framework.
// As this is server-only provider this function does nothing.
func (htp *Provider) SendAsync(request *request.Request) error { return nil }

func (htp *Provider) SendAsyncRaw(request *request.Request) error { return nil }

// SetHeaderDelimiter sets delimiter var for headers values.
// As this is server-only provider this function does nothing.
func (htp *Provider) SetHeaderDelimiter(delimiter string) {}

// SetConfig sets provider configuration and validates it.
func (htp *Provider) SetConfig(config interface{}) error {
	cfg, ok := config.(*Configuration)
	if !ok {
		return errors.ErrConfigurationIsNotForEcho
	}

	htp.cfg = cfg

	if err := htp.cfg.Validate(); err != nil {
		htp.logger.Error().Err(err).Msg("Failed to validate passed configuration")

		return err
	}

	return nil
}

// Starts HTTP server listening and ensures that it is ready for
// serving requests. This is a blocking call.
func (htp *Provider) startServer() error {
	if htp.cfg.UseDefaultLoggingMiddleware {
		htp.server.Use(htp.loggerMiddleware())
	}

	htp.logger = htp.logger.With().Str("address", htp.cfg.Address).Logger()

	go func() {
		err := htp.server.Start(htp.cfg.Address)

		if !strings.Contains(err.Error(), "Server closed") {
			htp.logger.Error().Err(err).Msg("HTTP server critical error occurred")
		}
	}()

	// Check that HTTP server was started.
	// As this is a client for checking there might (or might not) be a need in filling
	// all fields, so:
	// ToDo: check if we should fill all fields in client.
	// nolint:exhaustivestruct
	httpc := &http.Client{Timeout: time.Second * 1}
	checks := 0
	errorOccurred := false

	for {
		checks++

		if checks >= htp.cfg.WaitForSeconds {
			htp.logger.Error().Int("seconds passed", checks).Msg("HTTP server isn't up")

			errorOccurred = true

			break
		}

		time.Sleep(time.Second * 1)

		// ToDo: switch to context-aware function with timeout in context.
		//nolint:noctx
		resp, err := httpc.Get("http://" + htp.cfg.Address + "/_internal/waitForOnline")
		if err != nil {
			htp.logger.Debug().Err(err).Msg("HTTP error occurred, HTTP server isn't ready, waiting...")

			continue
		}

		response, err := ioutil.ReadAll(resp.Body)
		resp.Body.Close()

		if err != nil {
			htp.logger.Debug().Err(err).Msg("Failed to read response body, HTTP server isn't ready, waiting...")

			continue
		}

		htp.logger.Debug().Str("status", resp.Status).Int("body length", len(response)).Msg("HTTP response received")

		if resp.StatusCode == http.StatusOK {
			// This would appear only if echo is broken somehow and didn't
			// registered handler for /_internal/waitForOnline path.
			if len(response) == 0 {
				htp.logger.Warn().Msg("Response is empty, HTTP server isn't ready, waiting...")

				continue
			}

			htp.logger.Debug().Int("status code", resp.StatusCode).Msgf("Response: %+v", string(response))

			if len(response) == 17 {
				break
			}
		}
	}

	if errorOccurred {
		htp.logger.Error().Msg("Failed to start HTTP server")

		return errors.ErrUnableToStartHTTPServer
	}

	htp.logger.Info().Msg("HTTP server is ready to process requests")

	return nil
}

// Gracefully stops HTTP server and currently running requests.
func (htp *Provider) stopServer() error {
	htp.logger.Info().Msg("Stopping communication provider...")

	ctx := context.Background()

	return htp.server.Shutdown(ctx)
}

// UseMiddleware gives ability to add own middleware for HTTP server.
func (htp *Provider) UseMiddleware(middlewares ...echo.MiddlewareFunc) {
	htp.server.Use(middlewares...)
}

// Handler for checking if HTTP server is ready to process requests.
func (htp *Provider) waitForHTTPServerToBeUpHandler(ec echo.Context) error {
	response := map[string]string{
		"error": "None",
	}

	return ec.JSON(200, response)
}
