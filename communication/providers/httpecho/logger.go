package httpecho

import (
	"time"

	"github.com/labstack/echo/v4"
)

func (htp *Provider) loggerMiddleware() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(ec echo.Context) error {
			startTime := time.Now()

			err := next(ec)

			htp.logger.Info().
				Str("request.remote-ip", ec.RealIP()).
				Str("request.host", ec.Request().Host).
				Str("request.method", ec.Request().Method).
				Str("request.path", ec.Request().URL.Path).
				Str("request.remote-ua", ec.Request().UserAgent()).
				TimeDiff("response.time", time.Now(), startTime).
				Int64("response.size", ec.Response().Size).
				Msg("HTTP request")

			return err
		}
	}
}
