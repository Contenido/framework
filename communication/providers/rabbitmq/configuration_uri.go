package rabbitmq

import (
	"encoding/json"

	"gitlab.com/Contenido/framework/errors"
	"github.com/streadway/amqp"
)

// URI represents single RabbitMQ connection URI with possibility to extend it
// with additional features (like healthchecking, load-balancing, etc.).
type URI struct {
	uri string
}

func NewUri(uri string) *URI {

	return &URI{
		uri: uri,
	}
}

// UnmarshalJSON implements JSON Unmarshaler interface.
func (u *URI) UnmarshalJSON(data []byte) error {
	if err := json.Unmarshal(data, &u.uri); err != nil {
		return err
	}

	return nil
}

// Validate validates URI.
func (u *URI) Validate() error {
	// URI should be defined.
	if u.uri == "" {
		return errors.ErrRabbitMQConfigurationURIInvalid
	}

	// URI should be valid.
	if _, err := amqp.ParseURI(u.uri); err != nil {
		return errors.ErrRabbitMQConfigurationURIInvalid
	}

	return nil
}
