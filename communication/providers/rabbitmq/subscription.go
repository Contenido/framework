package rabbitmq

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"

	zl "github.com/rs/zerolog"
	"github.com/streadway/amqp"

	"gitlab.com/Contenido/framework/communication"
	"gitlab.com/Contenido/framework/communication/request"
)

const (
	RabbitMessageID         = "rabbit_message_id"
	RabbitMessageType       = "rabbit_message_type"
	RabbitMessageRoutingKey = "rabbit_message_routing_key"
)

// Internal to RabbitMQ provider subscription representation.
type subscription struct {
	amqpChannel     *amqp.Channel
	config          *Subscribe
	consumerName    string
	handler         communication.HandlerFunc
	logger          zl.Logger
	headerDelimiter string
}

// Consumes data and sends back via data channel for callback calling.
func (s *subscription) consume() {
	s.logger.Debug().Msg("Start consuming data...")

	// ToDo: figure out how to process this error properly.
	incomingData, _ := s.amqpChannel.Consume(s.config.QueueName, s.consumerName, true, false, true, true, nil)

	for data := range incomingData {
		// Request's details are set using setters, so:
		// nolint:exhaustivestruct
		req := &request.Request{}

		err := json.Unmarshal(data.Body, req)
		if err != nil {
			s.logger.Error().Err(err).Msg("Failed to parse incoming request, discarding it")

			continue
		}

		s.logger.Debug().Msgf("Got request: %+v", req)

		ctx := request.NewDefaultContext()

		// Check headers from req body firs.
		if req.Headers != nil {
			ctx.SetHeaders(req.Headers)
		}

		// If there are amqp headers.
		if len(data.Headers) > 0 {
			delimiter := ""
			if rawDelimiter, ok := data.Headers[defaultHeaderDelimiterKey]; ok {
				delimiter, _ = rawDelimiter.(string)
			}
			headersFromAMQP := request.NewHeadersFromInterfaceMapWithDelimiter(data.Headers, delimiter)
			for k, v := range headersFromAMQP {
				ctx.PutHeader(k, v)
			}
		}

		// Adding const headers for info from amqp.
		ctx.SetHeader(RabbitMessageID, data.MessageId)
		ctx.SetHeader(RabbitMessageType, data.Type)
		ctx.SetHeader(RabbitMessageRoutingKey, data.RoutingKey)

		ctx.SetContext(context.Background())
		ctx.SetFrom(req.From)
		ctx.SetPath(req.Path)
		ctx.SetProtocol("rabbitmq")
		ctx.SetBody(ioutil.NopCloser(bytes.NewBuffer(req.Data)))

		// ToDo: synchronous requests.
		// ToDo: handle this error.
		_ = s.handler(ctx)
	}
}

// Sets AMQP channel and (re)starts subscription.
func (s *subscription) startSubscription(channel *amqp.Channel) error {
	s.logger.Debug().Msg("Starting subscription...")

	s.amqpChannel = channel

	// Exchange declaring.
	if s.config.DeclareExchange {
		err := s.amqpChannel.ExchangeDeclare(s.config.Exchange, "direct", true, false, false, true, nil)
		if err != nil {
			s.logger.Error().Err(err).Msg("Failed to declare exchange!")

			return fmt.Errorf("rabbitmq communication provider: declare exchange: %w", err)
		}
	}

	// Queue declaring and binding.
	if s.config.DeclareQueue {
		_, err := s.amqpChannel.QueueDeclare(s.config.QueueName, true, false, false, true, nil)
		if err != nil {
			s.logger.Error().Err(err).Msg("Failed to declare queue!")

			return fmt.Errorf("rabbitmq communication provider: declare queue: %w", err)
		}
	}

	err := s.amqpChannel.QueueBind(s.config.QueueName, s.config.RoutingKey, s.config.Exchange, true, nil)
	if err != nil {
		s.logger.Error().Err(err).Msg("Failed to bind to queue!")

		return fmt.Errorf("rabbitmq communication provider: bind queue: %w", err)
	}

	go s.consume()

	return nil
}
