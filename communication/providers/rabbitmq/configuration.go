package rabbitmq

import "gitlab.com/Contenido/framework/errors"

// Configuration describes RabbitMQ provider general configuration. This
// structure should be used for connection configuration.
type Configuration struct {
	// DeclareExchange controls whenever RabbitMQ provider should try to
	// declare exchange.
	DeclareExchange bool `json:"declare_exchange"`
	// DeclareQueue controls whenever RabbitMQ provider should try to
	// declare queue.
	DeclareQueue bool `json:"declare_queue"`
	// Exchange is an exchange name to use.
	Exchange string `json:"exchange"`
	// ExchangeAutoDelete sets auto deletion flag on exchange.
	ExchangeAutoDelete bool `json:"exchange_auto_delete"`
	// ExchangeDurable sets durable flag on exchange.
	ExchangeDurable bool `json:"exchange_durable"`
	// ExchangeInternal sets internal flag on exchange.
	ExchangeInternal bool `json:"exchange_internal"`
	// ExchangeType is an exchange type.
	ExchangeType string `json:"exchange_type"`
	// ExchangeNoWait
	ExchangeNoWait bool `json:"exchange_nowait"`
	// URI is a connection string in form of "amqp://user:password@host:port".
	URI []*URI `json:"uri"`
}

// Validate validates configuration. Called by provider itself, no need to
// call it manually!
func (c *Configuration) Validate() error {
	// Validate all URIs.
	if len(c.URI) == 0 {
		return errors.ErrRabbitMQNoURIDefined
	}

	for _, uri := range c.URI {
		if err := uri.Validate(); err != nil {
			return err
		}
	}

	return nil
}

func (c *Configuration) AddURIsFromSliceOfStrings(uri []string) {
	for _, v := range uri {
		c.URI = append(c.URI, NewUri(v))
	}
}

// Subscribe describes subscription options.
type Subscribe struct {
	// DeclareExchange controls whenever RabbitMQ provider should try to
	// declare exchange.
	DeclareExchange bool
	// DeclareQueue controls whenever RabbitMQ provider should try to
	// declare queue.
	DeclareQueue bool
	// Exchange is an exchange name to use. Leave empty for default exchange
	// for application (e.g. APPNAME.comms).
	Exchange string
	// QueueName is a queue name.
	QueueName string
	// RoutingKey is a key which will be used to route messages within exchange.
	// This parameter might also be known as "key" from streadway/amqp docs
	// or "binding key" from examples.
	RoutingKey string
}
