package rabbitmq

import (
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"gitlab.com/Contenido/framework"
	"gitlab.com/Contenido/framework/communication"
	"gitlab.com/Contenido/framework/communication/request"
	"gitlab.com/Contenido/framework/communication/response"
	"gitlab.com/Contenido/framework/errors"
	"gitlab.com/Contenido/framework/internal/provider"
	"gitlab.com/Contenido/framework/logging"
	"gitlab.com/Contenido/framework/utils"
	zl "github.com/rs/zerolog"
	"github.com/streadway/amqp"
)

// ToDo: use this vhost!
// nolint
const (
	defaultRabbitMQVHost      = "microservices"
	defaultHeaderDelimiterKey = "request-header-delimiter"
)

// Provider provides RabbitMQ communication capabilities.
type Provider struct {
	cfg           *Configuration
	conn          *amqp.Connection
	app           *framework.Application
	subscriptions map[string]*subscription
	blockChan     chan amqp.Blocking
	closeChan     chan *amqp.Error
	consumerName  string
	provider.BaseProvider
	logger            zl.Logger
	connectionBlocked bool
	headerDelimiter   string
}

// NewProvider creates new RabbitMQ communication provider.
func NewProvider(app *framework.Application) *Provider {
	// As this is a provider instantiation there is no actual possibility to fill all
	// fields (they'll be filled with Initialize() call), so:
	// nolint:exhaustivestruct
	return &Provider{
		app: app,
	}
}

// Initialize performs preliminary initialization actions like internal
// structures initialization. Should not be called manually.
func (p *Provider) Initialize() error {
	p.consumerName = p.app.Configuration.Config.Framework.ApplicationName + "-" + utils.GenerateRandomString(16)

	logProvider, err := p.app.Logger.GetProvider(logging.DefaultLoggingProvider)
	if err != nil {
		return fmt.Errorf("rabbitmq communication provider: initialization: get logging provider: %w", err)
	}

	logger, err := logProvider.GetLogger("rabbitmq", nil)
	if err != nil {
		return fmt.Errorf("rabbitmq communication provider: initialization: get logger: %w", err)
	}

	p.logger = logger.(zl.Logger).With().Str("consumer", p.consumerName).Logger()

	p.subscriptions = make(map[string]*subscription)

	p.logger.Info().Msg("Initializing provider...")

	p.headerDelimiter = communication.DefaultHeadersDelimiter

	p.initializeConnectionChannels()

	p.RegisterStartFunc(p.startConnection)
	p.RegisterStopFunc(p.stopConnection)

	return nil
}

// (Re-)Initializes connection blocking and connection closed channels.
func (p *Provider) initializeConnectionChannels() {
	p.blockChan = make(chan amqp.Blocking)
	p.closeChan = make(chan *amqp.Error)
}

// IsClient always returns true as this is RabbitMQ *client* provider.
func (p *Provider) IsClient() bool { return true }

// IsClient always returns true as this is RabbitMQ *server* provider.
func (p *Provider) IsServer() bool { return true }

// RegisterHandler registers handler for communication channel. It will use
// following agreements:
//
//   * Subscription name (a.k.a. queue name and routing key) will be in form of
//     APPLICATIONNAME_METHOD_PATH (e.g. rates-api_POST_/api/v1/calculator/recalculate).
//   * Exchange name will always be APPLICATIONNAME.comms.
//   * Consumer name (that will appear in RMQ admin panel, for example) will always
//     be generated in form APPLICATIONNAME-RANDOMSTRING.
//
// Where:
//
//   * APPLICATIONNAME is a name of application, e.g. "rates-api" or "rates-calculator".
func (p *Provider) RegisterHandler(method string, path string, handler communication.HandlerFunc) error {
	subscriptionName := p.app.Configuration.Config.Framework.ApplicationName + "_" + method + "_" + path

	// nolint:exhaustivestruct
	sub := &subscription{
		config: &Subscribe{
			DeclareExchange: p.cfg.DeclareExchange,
			DeclareQueue:    p.cfg.DeclareQueue,
			Exchange:        p.app.Configuration.Config.Framework.ApplicationName + ".comms",
			QueueName:       subscriptionName,
			RoutingKey:      subscriptionName,
		},
		consumerName: p.consumerName,
		handler:      handler,
		logger:       p.logger.With().Str("subscription", subscriptionName).Logger(),
	}

	p.subscriptions[subscriptionName] = sub

	return nil
}

// Request sends request to remote thing in synchronous manner and returns
// either a response or error. Error here is a local error, not remote, it'll
// appear, for example, if we're unable to establish connection or we will
// fail to read response body.
func (p *Provider) Request(request *request.Request) (*response.Response, error) { return nil, nil }

// Send sends request to remote thing in synchronous manner and does not
// return anything but protocol error.
func (p *Provider) Send(request *request.Request) error {
	return nil
}

// SendAsync sends request to remote thing in asynchronous manner. It
// won't return any response or protocol errors, only internal for framework.
func (p *Provider) SendAsync(request *request.Request) error {
	request.From = p.app.Configuration.Config.Framework.ApplicationName

	amqpChannel, err := p.conn.Channel()
	if err != nil {
		p.logger.Error().Err(err).Msg("Failed to send message over RabbitMQ!")

		return fmt.Errorf("rabbitmq communication provider: send message asynchronously: get channel: %w", err)
	}
	defer amqpChannel.Close()

	requestAsBytes, err1 := json.Marshal(request)
	if err1 != nil {
		p.logger.Error().Err(err1).Msg("Failed to marshal request to JSON!")

		return fmt.Errorf("rabbitmq communication provider: send message asynchronously: marshal JSON: %w", err)
	}

	err2 := amqpChannel.Publish(
		request.To+".comms",
		request.To+"_"+request.Method+"_"+request.Path,
		true,
		false,
		// nolint:exhaustivestruct
		amqp.Publishing{
			ContentType:     "application/json",
			ContentEncoding: "utf-8",
			Body:            requestAsBytes,
		},
	)
	if err2 != nil {
		p.logger.Error().Err(err2).Msg("Failed to send message over RabbitMQ!")

		return fmt.Errorf("rabbitmq communication provider: send message asynchronously: publish: %w", err)
	}

	return nil
}

// SendAsyncRaw sends request to remote thing in asynchronous manner. It
// won't return any response or protocol errors, only internal for framework.
// That method sends request's Data field as message and puts Headers into provider's headers,
// if applicable, all other fields are ignored. request.To contains exchange,
// request.Path contains routing key, priority of message can be changed by specific header.
func (p *Provider) SendAsyncRaw(request *request.Request) error {
	amqpChannel, err := p.conn.Channel()
	if err != nil {
		p.logger.Error().Err(err).Msg("Failed to send message over RabbitMQ!")

		return fmt.Errorf("rabbitmq communication provider: send message asynchronously: get channel: %w", err)
	}
	defer amqpChannel.Close()

	// convert http request headers
	var amqpHeaders amqp.Table

	if request.Headers != nil {
		amqpHeaders = request.Headers.ToInterfaceMap(p.headerDelimiter)
	}

	if len(amqpHeaders) > 0 {
		amqpHeaders[defaultHeaderDelimiterKey] = p.headerDelimiter
	}

	err = amqpChannel.Publish(
		request.To,
		request.Path,
		true,
		false,
		// nolint:exhaustivestruct
		amqp.Publishing{
			Headers:      amqpHeaders,
			DeliveryMode: amqp.Persistent,
			ContentType:  "application/json",
			Priority:     p.getPriority(request.Headers),
			Body:         request.Data,
		},
	)
	if err != nil {
		p.logger.Error().Err(err).Msg("Failed to send message over RabbitMQ!")

		return fmt.Errorf("rabbitmq communication provider: send message asynchronously: publish: %w", err)
	}

	return nil
}

// SetHeaderDelimiter sets delimiter var for amqp headers values. amqp don't support arrays as header values,
// because of that each of http.Headers values should be joined in a string using delimiter var as separator.
func (p *Provider) SetHeaderDelimiter(delimiter string) {
	p.headerDelimiter = delimiter
}

// getPriority gets priority from headers if exist
func (p *Provider) getPriority(headers request.Headers) uint8 {
	if headers == nil {
		return 0
	}

	priority := 0

	if p, ok := headers["priority"]; ok {
		for _, v := range p {
			if num, err := strconv.Atoi(v); err == nil {
				priority = num
				break
			}
		}
	}

	if priority < 0 {
		priority = 0
	}

	if priority > 255 {
		priority = 255
	}

	return uint8(priority)
}

// SetConfig sets provider configuration and validates it.
func (p *Provider) SetConfig(config interface{}) error {
	cfg, ok := config.(*Configuration)
	if !ok {
		return errors.ErrNotAConfigurationStruct
	}

	p.cfg = cfg

	// Checks for configuration data.
	if err := p.cfg.Validate(); err != nil {
		p.logger.Error().Err(err).Msg("Failed to validate passed configuration")

		return err
	}

	return nil
}

// Start initializes connection.
func (p *Provider) startConnection() error {
	if p.conn != nil {
		err := p.conn.Close()
		if err != nil {
			p.logger.Warn().Err(err).Msg("Failed to close already opened connection. This is non-critical error.")
		}
	}

	if p.cfg == nil {
		return errors.ErrNoConfigurationRegistered
	}

	var (
		conn *amqp.Connection
		err  error
	)

	for _, uri := range p.cfg.URI {
		conn, err = amqp.Dial(uri.uri)
		if err == nil {
			break
		}

		p.logger.Error().Err(err).Msg("Failed to establish connection to RabbitMQ")
	}

	if conn == nil {
		p.logger.Error().Msg("Failed to establish connection to any of passed RabbitMQ URIs!")

		return errors.ErrRabbitMQConnectionEstablishingFailed
	}

	p.logger.Info().Msg("RabbitMQ connection established")

	p.conn = conn

	p.initializeConnectionChannels()

	go p.watchConnectionBlock()
	go p.watchConnectionClose()

	p.conn.NotifyBlocked(p.blockChan)
	p.conn.NotifyClose(p.closeChan)

	// Start subscriptions.
	for subscriptionName, sub := range p.subscriptions {
		p.logger.Debug().Str("path", subscriptionName).Msg("Starting RabbitMQ subscription...")

		amqpChannel, err := p.conn.Channel()
		if err != nil {
			p.logger.Error().Err(err).Str("path", subscriptionName).Msg("Failed to get channel for subscription!")
		}

		_ = sub.startSubscription(amqpChannel)
	}

	return nil
}

// Stop gracefully stops internal asynchronous worker.
func (p *Provider) stopConnection() error {
	p.logger.Info().Msg("Shutting down RabbitMQ connection...")
	// ToDo: wait for worker to be empty?

	return p.conn.Close()
}

// Watches for connection blocking events. This function should be run in
// different goroutine which will be run while connection is alive. When
// connection dies for some reason (network issues, server-initiated connection
// closing, etc.) connection re-establishing will be launched (via p.Start())
// which will re-create p.blockChan and restart this function.
func (p *Provider) watchConnectionBlock() {
	p.logger.Info().Msg("Starting RabbitMQ connection blocking watcher...")

	for msg := range p.blockChan {
		if msg.Active {
			p.logger.Warn().Str("reason", msg.Reason).Msg("TCP connection blocked")

			p.connectionBlocked = true
		} else {
			p.logger.Warn().Str("reason", msg.Reason).Msg("TCP connection unblocked")

			p.connectionBlocked = false
		}
	}
}

// Watches for connection close events. This function should be run in
// different goroutine and will receive only one message actually. When
// connection re-establishing will be launched p.closeChan will be closed
// and recreated which effectively will stop goroutine.
func (p *Provider) watchConnectionClose() {
	p.logger.Info().Msg("Starting RabbitMQ connection closing signal watcher...")

	for msg := range p.closeChan {
		p.logger.Warn().
			Err(msg).
			Bool("from_server", msg.Server).
			Int("code", msg.Code).
			Str("reason", msg.Reason).
			Msg("RabbitMQ connection is closed")

		for {
			err := p.Start()
			if err != nil {
				p.logger.Error().Err(err).Msg("Failed to re-establish connection to RabbitMQ")

				time.Sleep(time.Second * 1)

				continue
			}

			break
		}
	}
}
