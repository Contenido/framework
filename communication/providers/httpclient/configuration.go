package httpclient

import "gitlab.com/Contenido/framework/errors"

// Configuration is a structure that describes HTTP client configuration.
type Configuration struct {
	// Timeout is a request timeout.
	Timeout int64 `json:"timeout"`
}

// Validate validates configuration. Called by provider itself, no need to
// call it manually!
func (c *Configuration) Validate() error {
	// Timeout should always be greater than zero.
	if c.Timeout < 1 {
		return errors.ErrHTTPClientInvalidTimeout
	}

	return nil
}
