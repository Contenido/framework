package httpclient

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"time"

	"gitlab.com/Contenido/framework"
	"gitlab.com/Contenido/framework/communication"
	"gitlab.com/Contenido/framework/communication/request"
	"gitlab.com/Contenido/framework/communication/response"
	"gitlab.com/Contenido/framework/errors"
	"gitlab.com/Contenido/framework/internal/provider"
	"gitlab.com/Contenido/framework/logging"
	zl "github.com/rs/zerolog"
)

// Provider is a provider for HTTP client-to-server communication channel using
// standart http.Client.
type Provider struct {
	provider.BaseProvider

	client *http.Client
	app    *framework.Application
	config *Configuration
	logger zl.Logger
}

// NewProvider creates new HTTP client provider. It should be configured later
// (after configuration parsing) with Provider.SetConfig().
func NewProvider(app *framework.Application) *Provider {
	// As this is a provider instantiation there is no actual possibility to fill all
	// fields (they'll be filled with Initialize() call), so:
	// nolint:exhaustivestruct
	return &Provider{
		app: app,
	}
}

// Initialize performs preliminary initialization actions like internal
// structures initialization. Should not be called manually.
func (htp *Provider) Initialize() error {
	logProvider, err := htp.app.Logger.GetProvider(logging.DefaultLoggingProvider)
	if err != nil {
		return fmt.Errorf("httpclient provider: %w", err)
	}

	logger, err := logProvider.GetLogger("http client", nil)
	if err != nil {
		return fmt.Errorf("httpclient provider: %w", err)
	}

	htp.logger = logger.(zl.Logger)

	htp.logger.Info().Msg("Initializing provider...")

	// ToDo: check if we should fill client structure with additional data (probably have to).
	// nolint:exhaustivestruct
	htp.client = &http.Client{
		Timeout: time.Second * 10,
	}
	htp.config = &Configuration{Timeout: 10}

	return nil
}

// GetHTTPClient returns inner HTTP client for low-level operations and third-party
// requests.
func (htp *Provider) GetHTTPClient() *http.Client {
	return htp.client
}

// IsClient always returns true as this is HTTP *client* provider.
func (htp *Provider) IsClient() bool { return true }

// IsServer always returns false as this is HTTP *client* provider.
func (htp *Provider) IsServer() bool { return false }

// Processes HTTP request.
//nolint:unused
func (htp *Provider) processRequest(request *request.Request) ([]byte, error) {
	return nil, nil
}

// Processes HTTP request asynchronously using worker.
//nolint:unused
func (htp *Provider) processRequestAsynchronously(request interface{}) error {
	return nil
}

// RegisterHandler registers handler for communication channel. As this
// is a client - it silently does nothing.
func (htp *Provider) RegisterHandler(method string, path string, handler communication.HandlerFunc) error {
	return nil
}

// Request sends request to remote thing in synchronous manner and returns
// either a response or error. Error here is a local error, not remote, it'll
// appear, for example, if we're unable to establish connection or we will
// fail to read response body.
func (htp *Provider) Request(request *request.Request) (*response.Response, error) {
	to := time.Now().Add(time.Second * time.Duration(htp.config.Timeout))
	requestContext, cancel := context.WithDeadline(request.GetContext(), to)

	defer cancel()

	var Data io.Reader
	if request.Data != nil {
		Data = bytes.NewBuffer(request.Data)
	}

	httpURL := request.To + request.Path

	htp.logger.Debug().Str("URL", httpURL).Str("method", request.Method).Msg("HTTP URL for request")

	req, err := http.NewRequestWithContext(requestContext, request.Method, httpURL, Data)
	if err != nil {
		return nil, fmt.Errorf("httpclient provider: create new request: %w", err)
	}

	req.Header = request.Headers.ToHTTPHeader()

	resp, err := htp.client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("httpclient provider: execute request: %w", err)
	}

	responseBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("httpclient provider: reading response body: %w", err)
	}

	resp.Body.Close()

	htp.logger.Debug().Str("response", string(responseBody)).Msg("Got response")

	ourResponse := &response.Response{
		Body: ioutil.NopCloser(bytes.NewBuffer(responseBody)),
		Code: resp.StatusCode,
	}

	return ourResponse, nil
}

// Send sends request to remote thing in synchronous manner and does not
// return anything but protocol error.
func (htp *Provider) Send(request *request.Request) error { return nil }

// SendAsync sends request to remote thing in asynchronous manner. It
// won't return any response or protocol errors, only internal for framework.
func (htp *Provider) SendAsync(request *request.Request) error { return nil }

func (htp *Provider) SendAsyncRaw(request *request.Request) error { return nil }

// SetHeaderDelimiter sets delimiter var for headers values.
// Because this provider supports http.Headers, this function does nothing.
func (htp *Provider) SetHeaderDelimiter(delimiter string) {}

// SetConfig sets provider configuration, validates it and puts values from it
// into HTTP client.
func (htp *Provider) SetConfig(config interface{}) error {
	cfg, ok := config.(*Configuration)
	if !ok {
		return errors.ErrNotAConfigurationStruct
	}

	htp.config = cfg

	// Validate configuration.
	if err := htp.config.Validate(); err != nil {
		htp.logger.Error().Err(err).Msg("Failed to validate passed configuration")

		return err
	}

	htp.client.Timeout = time.Second * time.Duration(htp.config.Timeout)

	return nil
}
