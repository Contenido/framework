package response

import "io"

// Response represents generic service response structure for inter-service
// communication.
type Response struct {
	// Body is a response body.
	Body io.ReadCloser
	// Code is a response code (like in HTTP).
	Code int
}
