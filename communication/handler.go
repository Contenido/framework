package communication

import (
	"fmt"

	"gitlab.com/Contenido/framework/errors"
	"gitlab.com/Contenido/framework/internal/handler"
)

// Handler handles all communications channels application will use.
// Communication channels should be registered before application's
// business logic initialization in order to properly register
// handlers.
type Handler struct {
	handler.BaseHandler
}

// Initialize performs preliminary initialization actions like internal
// structures initialization.
func (h *Handler) Initialize() {}

// GetProvider returns provider by name.
func (h *Handler) GetProvider(providerName string) (Provider, error) {
	providerRaw, err := h.GetRawProvider(providerName)
	if err != nil {
		return nil, fmt.Errorf("communications handler: %w", err)
	}

	provider, ok := providerRaw.(Provider)
	if !ok {
		return nil, errors.ErrInvalidProvider
	}

	return provider, nil
}

// RegisterHandler registers handler for all communication channels.
func (h *Handler) RegisterHandler(providerName string, method string, path string, handler HandlerFunc) error {
	providerRaw, err := h.GetProvider(providerName)
	if err != nil {
		return err
	}

	provider, ok := providerRaw.(Provider)
	if !ok {
		return errors.ErrInvalidProvider
	}

	return provider.RegisterHandler(method, path, handler)
}

// SetHeaderDelimiters sets same headerDelimiter for all communication providers.
func (h *Handler) SetHeaderDelimiters(delimiter string) error {
	for _, providerName := range h.GetProvidersNames() {
		providerRaw, err := h.GetProvider(providerName)
		if err != nil {
			return err
		}
		provider, ok := providerRaw.(Provider)
		if !ok {
			return errors.ErrInvalidProvider
		}
		provider.SetHeaderDelimiter(delimiter)
	}

	return nil
}
