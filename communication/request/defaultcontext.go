package request

import (
	"context"
	"io"
)

// OutFunc is a function signature for data output.
type OutFunc func(code int, data interface{}, additionals ...interface{}) error

// DefaultContext is an incoming request context. If you're implementing your
// own request context structure - do not forget to embed it into your structure!
type DefaultContext struct {
	body        io.ReadCloser
	ctx         context.Context
	from        string
	path        string
	protocol    string
	headers     Headers
	providerCtx interface{}

	jsonOutFunc   OutFunc
	stringOutFunc OutFunc
}

func NewDefaultContext() *DefaultContext {
	dc := DefaultContext{
		headers: make(Headers),
	}

	return &dc
}

// AddHeader appends new value for header.
func (dc *DefaultContext) AddHeader(key, value string) { dc.headers.Add(key, value) }

// Body is a body of request.
func (dc *DefaultContext) Body() io.ReadCloser { return dc.body }

// Context returns context.Context instance for later use with other
// parts of application (like database queries).
func (dc *DefaultContext) Context() context.Context {
	return dc.ctx
}

// From returns origin identificator. This could be service name
// or complete PROTO://ADDR line, depending on communication provider
// in use.
func (dc *DefaultContext) From() string { return dc.from }

// GetHeaderValue returns value from single header for request.
func (dc *DefaultContext) GetHeaderValue(key string) string { return dc.headers.Get(key) }

// GetHeaderValues returns all values from single header.
func (dc *DefaultContext) GetHeaderValues(key string) []string { return dc.headers.Values(key) }

// Headers returns all headers for request.
func (dc *DefaultContext) Headers() Headers { return dc.headers }

// Path returns complete path on which request was made. For example
// it can be /api/v1/YOUNAMEIT.
func (dc *DefaultContext) Path() string { return dc.path }

// Protocol returns protocol name for request. For example it can be set
// to "http" or "rabbitmq".
func (dc *DefaultContext) Protocol() string { return dc.protocol }

// ProviderContext returns interface to provider Context object.
func (dc *DefaultContext) ProviderContext() interface{} { return dc.providerCtx }

// SetBody sets body field.
func (dc *DefaultContext) SetBody(body io.ReadCloser) {
	dc.body = body
}

// SetContext sets request's context.
func (dc *DefaultContext) SetContext(ctx context.Context) { dc.ctx = ctx }

// SetFrom sets "From" field.
func (dc *DefaultContext) SetFrom(from string) { dc.from = from }

// SetHeader sets single new value to header overwriting all other values.
func (dc *DefaultContext) SetHeader(key, value string) { dc.headers.Set(key, value) }

// PutHeader sets new values to header overwriting all other values.
func (dc *DefaultContext) PutHeader(key string, value []string) { dc.headers.Put(key, value) }

// SetHeaders sets "Headers" field.
func (dc *DefaultContext) SetHeaders(headers Headers) { dc.headers = headers }

// SetPath sets "Path" field.
func (dc *DefaultContext) SetPath(path string) { dc.path = path }

// SetProtocol sets "Protocol" field.
func (dc *DefaultContext) SetProtocol(protocol string) { dc.protocol = protocol }

// SetProviderContext sets provider-specific context for later usage.
func (dc *DefaultContext) SetProviderContext(ctx interface{}) {
	dc.providerCtx = ctx
}

// SetWriteJSONFunc sets JSON writing function from provider.
func (dc *DefaultContext) SetWriteJSONFunc(outputFunc OutFunc) { dc.jsonOutFunc = outputFunc }

// SetWriteStringFunc sets string writing function from provider.
func (dc *DefaultContext) SetWriteStringFunc(outputFunc OutFunc) { dc.stringOutFunc = outputFunc }

// WriteJSON writes JSON back, if applicable by provider.
func (dc *DefaultContext) WriteJSON(code int, data interface{}, additionals ...interface{}) error {
	return dc.jsonOutFunc(code, data, additionals...)
}

// WriteString writes string back to client, if applicable by provider.
func (dc *DefaultContext) WriteString(code int, data string, additionals ...interface{}) error {
	return dc.stringOutFunc(code, data, additionals...)
}
