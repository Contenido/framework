package request

import (
	"context"
	"encoding/json"
)

// Request represents generic request structure which is used for inter-service
// communication.
type Request struct {
	// Request's context.
	ctx context.Context

	// From is a service name from which request was received.
	From string `json:"from"`
	// To is a service name we will use for request. Can be IP, domain
	// or something we will ask Consul about. For IP/domain protocol
	// should be included.
	To string `json:"to"`
	// Method is a HTTP method to use.
	Method string `json:"method"`
	// Path is a path on which request will be sent. Should be in form
	// of "/api/vVERSION/endpoint".
	Path string `json:"path"`
	// Data is a data that will be passed to remote service.
	Data json.RawMessage `json:"data"`

	// Optional flags.
	// IsAsynchronous indicates that request should be processed asynchronously
	// and sender doesn't want to receive reply.
	IsAsynchronous bool `json:"is_asynchronous"`
	// SendOnlyData sends only Data field as payload, not this entire request.
	SendOnlyData bool `json:"-"`

	// Optional fields.
	// Headers is a map that contains additional headers that might be used by remote system.
	Headers Headers `json:"headers,omitempty"`
}

func (r *Request) GetContext() context.Context {
	return r.ctx
}

func (r *Request) SetContext(ctx context.Context) {
	r.ctx = ctx
}
