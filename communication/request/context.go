package request

import (
	"context"
	"io"
)

// Context is a generic request context that will contain everything
// every request might need.
// Every communication provider SHOULD have own functions for filling
// this context. Every handler SHOULD use this context.
// This context is for servers only, client communication providers
// should not implement it.
type Context interface {
	// AddHeader appends new value for header.
	AddHeader(key, value string)
	// Body is a body of request.
	Body() io.ReadCloser
	// Context returns context.Context instance for later use with other
	// parts of application (like database queries).
	Context() context.Context
	// From returns origin identificator. This could be service name
	// or complete PROTO://ADDR line, depending on communication provider
	// in use.
	From() string
	// GetHeaderValue returns value from single header for request.
	GetHeaderValue(key string) string
	// GetHeaderValues returns all values from single header.
	GetHeaderValues(key string) []string
	// Headers returns Headers structure which contains all known headers for
	// this request. It's API pretty similar to http.Header.
	Headers() Headers
	// Path returns complete path on which request was made. For example
	// it can be /api/v1/YOUNAMEIT.
	Path() string
	// Protocol returns protocol name for request. For example it can be set
	// to "http" or "rabbitmq".
	Protocol() string
	// ProviderContext returns interface to provider Context object.
	ProviderContext() interface{}
	// SetHeader sets new value to header overwriting all other values.
	SetHeader(key, value string)
	// WriteJSON writes JSON back, if applicable by provider.
	WriteJSON(code int, data interface{}, additionals ...interface{}) error
	// WriteString writes string back to client, if applicable by provider.
	WriteString(code int, data string, additionals ...interface{}) error
}
