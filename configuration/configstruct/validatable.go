package configstruct

// ValidatableServiceConfig is an interface for dynamically registered service
// configuration structure.
type ValidatableServiceConfig interface {
	Validate() error
}
