package configstruct

import (
	"os"
	"path/filepath"
)

// Framework describes framework and it's subsystems and providers
// configuration. Note that there are a possibility to use one provider
// several times so this structure isn't describing complete configuration,
// only that is really should be specified once. Also every provider has it's
// own configuration that should be registered as part of service-specific
// configuration.
type Framework struct {
	// ApplicationName is a name of application. If not defined in configuration
	// binary name will be taken.
	ApplicationName string `json:"application_name"`
	// ApplicationVersion is a version of application. If not defined in configuration
	// a value "0.0.0" will be used.
	ApplicationVersion string `json:"application_version"`
	// Logger describes common logging configuration.
	Logger struct {
		// Level is a logging level to use. By default application will start
		// with 'info' level which can be overridden here. This variable is
		// case-insensitive.
		Level string
	}
	// TasksWorker defines tasks worker pool configuration.
	TasksWorker struct {
		// Logger defines additional logging abilities that is used within tasks
		// worker pool.
		Logger struct {
			// LogQueueRecreations allows to log queues recreations in different
			// situations.
			LogQueueRecreations bool
			// LogWorkerIterations allows to log worker iterations messages like
			// "there is nothing to run" and such.
			LogWorkerIterations bool
		}
	}
}

// Validate validates configuration values. This version also checks if default
// values was defined because they're necessary to get application running. This
// function should not be called manually as it is called by configuration
// subsystem automatically when InitializePost() is called.
func (f *Framework) Validate() error {
	// Get name of binary if application name isn't defined.
	if f.ApplicationName == "" {
		f.ApplicationName = filepath.Base(os.Args[0])
	}

	if f.ApplicationVersion == "" {
		f.ApplicationVersion = "0.0.0"
	}

	if f.Logger.Level == "" {
		f.Logger.Level = "INFO"
	}

	return nil
}
