package configstruct

import (
	"os"
	"reflect"
	"strconv"
)

// Struct is a configuration of whole application. It consists of
// framework configuration and application configuration, later is
// a dynamically registered thing.
type Struct struct {
	// Framework is a framework configuration.
	Framework *Framework `json:"framework"`
	// Service is a dynamically registered service configuration. Pointer
	// to structure should be behind interface{}!
	Service interface{} `json:"service"`
}

// CompareAndFill compares original structure and filled by configuration
// provider copy of it and fills original structure fields if they
// contain different values. This function is a helper to configuration
// providers and should not be called manually at all!
func (s *Struct) CompareAndFill(filledConfig *Struct) {
	var allowDefaults bool

	allowDefaultsFromEnvAsString, found := os.LookupEnv("CONFIG_ALLOW_DEFAULTS")
	if found {
		allowDefaultsFromEnv, err := strconv.ParseBool(allowDefaultsFromEnvAsString)
		if err == nil {
			allowDefaults = allowDefaultsFromEnv
		}
	}

	s.compareAndFillRecursive(reflect.ValueOf(s.Framework), reflect.ValueOf(filledConfig.Framework), allowDefaults)
	s.compareAndFillRecursive(reflect.ValueOf(s.Service), reflect.ValueOf(filledConfig.Service), allowDefaults)
}

// Recursively (if needed) fills structure. It will pass silently if
// error appeared or values will match. The "allowDefaults" flag allows
// default values for each type to be assigned if value already has
// non-default value (e.g. assigning 'false' when boolean value already
// 'true').
// nolint:cyclop
func (s *Struct) compareAndFillRecursive(original, filledCopy reflect.Value, allowDefaults bool) {
	//nolint:exhaustive
	switch original.Kind() {
	case reflect.Ptr, reflect.Interface:
		// For pointers we just get rid of them (taking original element
		// on which pointer points) and re-run recursive filling. Same for
		// interface{} things.
		s.compareAndFillRecursive(original.Elem(), filledCopy.Elem(), allowDefaults)
	case reflect.Struct:
		// For struct fields we should (again) re-run recursive filling.
		// Real fields will be filled below.
		for i := 0; i < original.NumField(); i++ {
			s.compareAndFillRecursive(original.Field(i), filledCopy.Field(i), allowDefaults)
		}
	case reflect.String:
		// Get both strings and compare them. Original string will only
		// be replaced if filled one is not empty and when they both do not
		// match each other.
		originalString := original.Interface().(string)
		filledString := filledCopy.Interface().(string)

		if !allowDefaults && filledString == "" {
			return
		}

		if originalString != "" && originalString == filledString {
			return
		}

		original.SetString(filledString)
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		// Integers in original structure should be replaced only if original
		// and filled do not match.
		originalInt := original.Int()
		filledInt := filledCopy.Int()

		if !allowDefaults && filledInt == 0 {
			return
		}

		if originalInt != filledInt {
			original.SetInt(filledInt)
		}
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		// Unsigned integers in original structure should be replaced only if
		// original and filled do not match.
		originalUint := original.Uint()
		filledUint := filledCopy.Uint()

		if !allowDefaults && filledUint == uint64(0) {
			return
		}

		if originalUint != filledUint {
			original.SetUint(filledUint)
		}
	case reflect.Float32, reflect.Float64:
		// Floating point numbers in original structure should be replaced
		// only if original and filled do not match.
		originalFloat := original.Float()
		filledFloat := filledCopy.Float()

		if !allowDefaults && filledFloat == float64(0.0) {
			return
		}

		if originalFloat != filledFloat {
			original.SetFloat(filledFloat)
		}
	case reflect.Slice:
		// ToDo: figure out how to deal with slices.
	case reflect.Map:
		// Don't do anything for empty maps.
		if len(filledCopy.MapKeys()) == 0 {
			return
		}

		// For maps we should compare values and update them if they're
		// do not match. Also there might be new keys in filled structure
		// map so we should take filled structure's map as point of origin.
		for _, key := range filledCopy.MapKeys() {
			originalValue := original.MapIndex(key)

			if original.MapIndex(key).IsZero() {
				// Empty map values in original map indicates that there are
				// no such key defined. Create it.
				original.SetMapIndex(key, originalValue)
			}

			filledCopyValue := filledCopy.MapIndex(key)

			// Note: map values might be even interface{}! So re-run
			// recursive filling.
			s.compareAndFillRecursive(originalValue, filledCopyValue, allowDefaults)
		}
	}
}

// Copy returns empty copy of configuration structure for later usage
// and returns it. This function is a helper for configuration providers
// and should not be called manually at all!
func (s *Struct) Copy() Struct {
	return Struct{
		// nolint:exhaustivestruct
		Framework: &Framework{},
		Service:   s.Service,
	}
}
