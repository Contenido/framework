package configuration

import (
	"fmt"
	"reflect"

	"gitlab.com/Contenido/framework/configuration/configstruct"
	"gitlab.com/Contenido/framework/errors"
	"gitlab.com/Contenido/framework/internal/handler"
)

// Handler handles all configuration things application will use.
// Configuration providers should be registered before application's
// business logic initialization in order to be properly used.
type Handler struct {
	Config *configstruct.Struct

	handler.BaseHandler
}

// Initialize initializes handler.
func (h *Handler) Initialize() {
	h.Config = &configstruct.Struct{
		// nolint:exhaustivestruct
		Framework: &configstruct.Framework{},
		Service:   nil,
	}
}

// RegisterServiceConfiguration registers service-specific configuration
// structure for parsing. This function should be called before Start()!
func (h *Handler) RegisterServiceConfig(config interface{}) error {
	// Passed value should be a pointer to structure.
	if reflect.ValueOf(config).Kind() != reflect.Ptr && reflect.ValueOf(config).Elem().Kind() != reflect.Struct {
		return errors.ErrNotAPtr
	}

	// Passed value should be validatable.
	_, ok := config.(configstruct.ValidatableServiceConfig)
	if !ok {
		return errors.ErrConfigurationIsNotValidatable
	}

	h.Config.Service = config

	// Set pointer in providers.
	// This is a sane sacrifice, because implementing it in other way would require
	// a separate function for configuration parsing which might not be obvious to
	// understand in terms of framework architecture.
	providerNames := h.GetProvidersNames()

	for _, name := range providerNames {
		provider, err := h.GetRawProvider(name)
		if err != nil {
			return fmt.Errorf("configuration handler: %w", err)
		}

		err = provider.SetConfig(h.Config)
		if err != nil {
			return fmt.Errorf("configuration handler: %w", err)
		}
	}

	return nil
}
