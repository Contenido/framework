package envconfig

import (
	"fmt"

	"gitlab.com/Contenido/framework/configuration/configstruct"
	"gitlab.com/Contenido/framework/errors"
	"gitlab.com/Contenido/framework/internal/provider"

	"github.com/vrischmann/envconfig"
)

// Provider provides ability to define configuration in environment variables.
// It use github.com/vrischmann/envconfig internally for that.
type Provider struct {
	structure *configstruct.Struct
	provider.BaseProvider
}

// Initialize initializes provider's internal state.
func (p *Provider) Initialize() error {
	p.RegisterStartFunc(p.parse)

	return nil
}

// Parses environment variables (if defined) into configuration structure.
func (p *Provider) parse() error {
	if p.structure == nil {
		return errors.ErrConfigurationIsNotSet
	}

	cfgCopy := p.structure.Copy()

	err := envconfig.InitWithOptions(
		&cfgCopy,
		envconfig.Options{
			AllowUnexported: true,
			AllOptional:     true,
			LeaveNil:        false,
			Prefix:          "",
		},
	)
	if err != nil {
		return fmt.Errorf("envconfig configuration provider: parse: %w", err)
	}

	p.structure.CompareAndFill(&cfgCopy)

	return nil
}

// SetConfig sets configuration we will use.
func (p *Provider) SetConfig(config interface{}) error {
	cfg, ok := config.(*configstruct.Struct)
	if !ok {
		return errors.ErrNotAConfigurationStruct
	}

	p.structure = cfg

	return nil
}
