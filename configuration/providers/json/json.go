package json

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/Contenido/framework/configuration/configstruct"
	"gitlab.com/Contenido/framework/errors"
	"gitlab.com/Contenido/framework/internal/provider"
	"gitlab.com/Contenido/framework/utils"
)

const (
	JSONConfigEnvVarName = "JSON_CONFIG"
	JSONConfigFlagName   = "json"
)

// Provider provides ability to define configuration in JSON. It uses standart
// encoding/json internally.
type Provider struct {
	configPath string
	structure  *configstruct.Struct

	provider.BaseProvider
}

// Checks if flags from CLI was parsed.
func (p *Provider) checkFlag() error {
	if !flag.Parsed() {
		flag.Parse()
	}

	return nil
}

// checkEnv Checks JSONConfigEnvVarName for file path, if env exist, rewrites value passed in flag.
func (p *Provider) checkEnv() error {
	if v, ok := os.LookupEnv(JSONConfigEnvVarName); ok {
		p.configPath = v
	}

	return nil
}

// Checks if flags from CLI was parsed.
func (p *Provider) checkConfigPath() error {
	if p.configPath == "" {
		return errors.ErrConfigurationFilePathIsEmpty
	}

	return nil
}

// Initialize initializes provider's internal state.
func (p *Provider) Initialize() error {
	flag.StringVar(&p.configPath, JSONConfigFlagName, "", "Path to JSON-formatted configuration")

	p.RegisterStartFunc(p.checkFlag)
	p.RegisterStartFunc(p.checkEnv)
	p.RegisterStartFunc(p.checkConfigPath)
	p.RegisterStartFunc(p.parse)

	return nil
}

// Parses configuration from JSON file into configuration structure.
func (p *Provider) parse() error {
	if p.structure == nil {
		return errors.ErrConfigurationIsNotSet
	}

	normalizedPath, err := utils.NormalizeFilePath(p.configPath)
	if err != nil {
		return fmt.Errorf("json provider: %w", err)
	}

	configFileData, err1 := ioutil.ReadFile(normalizedPath)
	if err1 != nil {
		return fmt.Errorf("json provider: %w", err1)
	}

	cfgCopy := p.structure.Copy()

	err2 := json.Unmarshal(configFileData, &cfgCopy)
	if err2 != nil {
		return fmt.Errorf("json provider: %w", err2)
	}

	p.structure.CompareAndFill(&cfgCopy)

	return nil
}

// SetConfig sets configuration we will work with.
func (p *Provider) SetConfig(config interface{}) error {
	cfg, ok := config.(*configstruct.Struct)
	if !ok {
		return errors.ErrNotAConfigurationStruct
	}

	p.structure = cfg

	return nil
}
