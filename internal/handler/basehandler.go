package handler

import (
	"context"
	"fmt"
	"sync"

	"gitlab.com/Contenido/framework/errors"
	"gitlab.com/Contenido/framework/internal/provider"
)

// BaseHandler is an embeddable structure for all subsystems handlers. It contains everything
// they have in common.
type BaseHandler struct {
	providers      map[string]provider.BaseInterface
	providersMutex sync.RWMutex

	globalCtx context.Context
}

// GetRawProvider returns requested provider by name. Returns raw provider which is
// provider.BaseInterface and should be transformed into needed interface by actual handler.
// Should not be called by hands in application's code unless you're know what you're doing!
func (bh *BaseHandler) GetRawProvider(providerName string) (provider.BaseInterface, error) {
	bh.providersMutex.RLock()
	defer bh.providersMutex.RUnlock()

	if len(bh.providers) == 0 {
		return nil, errors.ErrNoProvidersRegistered
	}

	provider, found := bh.providers[providerName]
	if !found {
		return nil, errors.ErrProviderNotFound
	}

	return provider, nil
}

// GetProvidersNames returns a slice of strings with names of registered providers.
func (bh *BaseHandler) GetProvidersNames() []string {
	bh.providersMutex.RLock()
	defer bh.providersMutex.RUnlock()

	names := make([]string, 0, len(bh.providers))

	for name := range bh.providers {
		names = append(names, name)
	}

	return names
}

// RegisterProvider registers new communication provider.
func (bh *BaseHandler) RegisterProvider(name string, p provider.BaseInterface) error {
	bh.providersMutex.Lock()
	defer bh.providersMutex.Unlock()

	if bh.providers == nil {
		bh.providers = make(map[string]provider.BaseInterface)
	}

	if _, found := bh.providers[name]; found {
		return errors.ErrProviderAlreadyRegistered
	}

	err := p.Initialize()
	if err != nil {
		return fmt.Errorf("basehandler: register provider: initialize provider: %w", err)
	}

	bh.providers[name] = p

	return nil
}

// SetConfig sets provider-specific configuration.
func (bh *BaseHandler) SetConfig(providerName string, config interface{}) error {
	bh.providersMutex.RLock()
	defer bh.providersMutex.RUnlock()

	if len(bh.providers) == 0 {
		return errors.ErrNoProvidersRegistered
	}

	provider, found := bh.providers[providerName]

	if !found {
		return errors.ErrProviderNotFound
	}

	return provider.SetConfig(config)
}

// SetContext sets context that will be passed to every provider whenever possible.
func (bh *BaseHandler) SetContext(ctx context.Context) {
	bh.globalCtx = ctx
}

// Start passes global context to provider and tries to start it.
func (bh *BaseHandler) Start(providerName string) error {
	provider, err := bh.GetRawProvider(providerName)
	if err != nil {
		return err
	}

	provider.SetContext(bh.globalCtx)

	return provider.Start()
}

// StartAll starts all registered providers.
func (bh *BaseHandler) StartAll() error {
	var (
		err       error
		providers []string
	)

	bh.providersMutex.RLock()
	for name := range bh.providers {
		providers = append(providers, name)
	}
	bh.providersMutex.RUnlock()

	for _, name := range providers {
		err = bh.Start(name)
		if err != nil {
			err = fmt.Errorf("provider %s: %w", name, err)

			break
		}
	}

	return err
}

// Stop stops specific provider.
func (bh *BaseHandler) Stop(providerName string) []error {
	provider, err := bh.GetRawProvider(providerName)
	if err != nil {
		return []error{err}
	}

	errs := provider.Shutdown()
	if len(errs) > 0 {
		return errs
	}

	return nil
}

// StopAll stops all registered providers.
func (bh *BaseHandler) StopAll() []error {
	bh.providersMutex.RLock()
	defer bh.providersMutex.RUnlock()

	for _, provider := range bh.providers {
		errs := provider.Shutdown()
		if len(errs) > 0 {
			return errs
		}
	}

	return nil
}
