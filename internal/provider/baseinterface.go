package provider

import "context"

// BaseInterface describes an interface every provider should conform. Any custom
// provider interface should embed this one.
type BaseInterface interface {
	// BaseProviderInitialize initializes internal state for base provider. It should be
	// called by provider itself and not by external user.
	BaseProviderInitialize()
	// GetContext returns provider's context. Should be used by providers itself (mostly).
	GetContext() context.Context
	// GetName returns name of provider that was previously set by SetName().
	GetName() string
	// Initialize initializes provider's internal state.
	Initialize() error
	// RegisterStartFunc registers starting function to execute.
	RegisterStartFunc(Func)
	// RegisterStopFunc registers a function to execute on application shutdown.
	RegisterStopFunc(Func)
	// SetConfig sets provider's configuration.
	SetConfig(interface{}) error
	// SetContext sets provider's context.
	SetContext(ctx context.Context)
	// SetName sets provider's name. Should be called ASAP.
	SetName(string)
	// Shutdown calls all registered shutdown functions and return all errors appeared.
	Shutdown() []error
	// Start calls all registered starting functions and return appeared error immediately.
	Start() error
}
