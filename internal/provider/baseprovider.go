package provider

import (
	"context"
	"sync"
)

// Func is a function that executed on provider's start or on shutdown.
// Takes no parameters and might return error.
type Func func() error

// BaseProvider is an embeddable structure which describes what every provider will contain.
// This structure should be initialized by calling BaseProviderInitialize() function to
// properly populate it's internal state. This call should reside right after SetName()
// and SetLoggerParams() (if they're needed).
// Before calling Start() ensure that you've registered starting functions with RegisterStartFunc().
// Also before shutdown you should register a shutdown functions with RegisterShutdownFunc().
type BaseProvider struct {
	name string
	ctx  context.Context

	initializePostFuncs []Func
	initializePreFuncs  []Func
	startFuncs          []Func
	shutdownFuncs       []Func

	providerStarted      bool
	providerStartedMutex sync.RWMutex
}

// BaseProviderInitialize initializes internal state.
func (bp *BaseProvider) BaseProviderInitialize() {
	bp.initializePostFuncs = make([]Func, 0)
	bp.initializePreFuncs = make([]Func, 0)
	bp.startFuncs = make([]Func, 0)
	bp.shutdownFuncs = make([]Func, 0)
}

// GetContext returns provider's context. Should be used by providers itself (mostly).
func (bp *BaseProvider) GetContext() context.Context {
	return bp.ctx
}

// GetName returns name of provider that was previously set by SetName().
func (bp *BaseProvider) GetName() string {
	return bp.name
}

// IsStarter returns true if Start() was called.
func (bp *BaseProvider) IsStarted() bool {
	bp.providerStartedMutex.RLock()
	defer bp.providerStartedMutex.RUnlock()

	return bp.providerStarted
}

// RegisterStartFunc registers starting function to execute.
func (bp *BaseProvider) RegisterStartFunc(f Func) {
	bp.startFuncs = append(bp.startFuncs, f)
}

// RegisterStopFunc registers a function to execute on application shutdown.
func (bp *BaseProvider) RegisterStopFunc(f Func) {
	bp.shutdownFuncs = append(bp.shutdownFuncs, f)
}

// SetContext sets provider's context.
func (bp *BaseProvider) SetContext(ctx context.Context) {
	bp.ctx = ctx
}

// SetName sets provider's name. Should be called ASAP.
func (bp *BaseProvider) SetName(name string) {
	bp.name = name
}

// Shutdown calls all registered shutdown functions and return all errors appeared.
func (bp *BaseProvider) Shutdown() []error {
	var errs []error

	for _, f := range bp.shutdownFuncs {
		err := f()
		if err != nil {
			errs = append(errs, err)
		}
	}

	return errs
}

// Start calls all registered starting functions and return appeared error immediately.
func (bp *BaseProvider) Start() error {
	for _, f := range bp.startFuncs {
		err := f()
		if err != nil {
			return err
		}
	}

	bp.providerStartedMutex.Lock()
	bp.providerStarted = true
	bp.providerStartedMutex.Unlock()

	return nil
}
