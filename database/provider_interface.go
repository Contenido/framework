package database

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/Contenido/framework/internal/provider"
)

type Provider interface {
	provider.BaseInterface

	// CommitOrRollbackTransaction tries to commit transaction and return error if something
	// goes wrong (e.g. transaction was invalidated on failed INSERT/UPDATE).
	CommitOrRollbackTransaction(tx *sqlx.Tx) error
	// GetConnection returns slave connection pointer for later use. In case when
	// only MasterDSN was specified in configuration - connection to master will
	// be returned.
	GetConnection() (*sqlx.DB, error)
	// GetDialect returns name of dialect for things like database migration.
	GetDialect() string
	// GetMasterConnection returns master connection pointer for later use. It
	// should be used only for writing or updating, e.g. database migrations.
	GetMasterConnection() (*sqlx.DB, error)
	// GetTransaction returns SQL transaction thing using connection to master.
	GetTransaction() (*sqlx.Tx, error)
	// SetConfig sets provider's connection configuration.
	SetConfig(config interface{}) error
	// Start starts provider work. This is blocking call and will
	// return only if provider starting sequence will be successfully
	// completed or completely failed.
	Start() error
	// Stop stops provider from doing it's works. This is blocking call and
	// will return only if provider stopping sequence will be successfully
	// completed or completely failed.
	Stop() error
}
