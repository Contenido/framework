package postgresql

import (
	"gitlab.com/Contenido/framework/database/providers/baseprovider"
)

// Configuration describes postgresql connection configuration.
type Configuration struct {
	baseprovider.Configuration
}

// Validate validates configuration. Called by provider itself, no need to
// call it manually!
func (c *Configuration) Validate() error {
	// Nothing to validate here.
	return nil
}
