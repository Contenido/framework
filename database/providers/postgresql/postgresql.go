package postgresql

import (
	// PostgreSQL driver.
	_ "github.com/jackc/pgx/v4/stdlib"
	"gitlab.com/Contenido/framework"
	"gitlab.com/Contenido/framework/database/providers/baseprovider"
	"gitlab.com/Contenido/framework/internal/provider"
)

// Provider provides PostgreSQL connection (and everything related, like
// transactions) handling.
type Provider struct {
	baseprovider.Base
}

// NewProvider creates new PostgreSQL database connection provider.
func NewProvider(app *framework.Application, name string) *Provider {
	p := &Provider{Base: baseprovider.Base{BaseProvider: provider.BaseProvider{}}}
	p.SetApplication(app)
	p.SetDialect("pgx")
	p.SetName(name)

	return p
}

// Initialize initializes provider's internal state.
func (p *Provider) Initialize() error {
	return p.InitializeBase()
}
