package baseprovider

import (
	"fmt"
	"math/rand"
	"sync"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/Contenido/framework"
	"gitlab.com/Contenido/framework/errors"
	"gitlab.com/Contenido/framework/internal/provider"
	"gitlab.com/Contenido/framework/logging"
	zl "github.com/rs/zerolog"
)

// Base contains some common things for all database providers, e.g. master-slave
// handling. When embedding this structure make sure you're issued SetApplication(),
// SetDialect() and SetName(), otherwise bad things may occur.
type Base struct {
	config     Configurationer
	randomer   *rand.Rand
	app        *framework.Application
	masterConn *sqlx.DB
	name       string
	dialect    string
	provider.BaseProvider
	logger              zl.Logger
	slaveConns          []*sqlx.DB
	shouldShutdownMutex sync.RWMutex
	shouldShutdown      bool
}

// Returns value of shouldShutdown flag in safe manner.
func (b *Base) areWeShuttingDown() bool {
	b.shouldShutdownMutex.RLock()
	defer b.shouldShutdownMutex.RUnlock()

	return b.shouldShutdown
}

// CommitOrRollbackTransaction tries to commit transaction and return error if something
// goes wrong (e.g. transaction was invalidated on failed INSERT/UPDATE).
func (b *Base) CommitOrRollbackTransaction(tx *sqlx.Tx) error {
	if err := tx.Commit(); err != nil {
		b.logger.Error().Err(err).Msg("Failed to commit transaction! Will try to roll back")

		err1 := tx.Rollback()
		if err1 != nil {
			b.logger.Error().Err(err1).Msg("Failed to rollback transaction! Database might be inconsistent!")

			return fmt.Errorf("database provider: commit or rollback: rollback: %w", err1)
		}

		return fmt.Errorf("database provider: commit or rollback: commit: %w", err1)
	}

	return nil
}

// GetConnection returns slave connection pointer for later use. In case when
// only MasterDSN was specified in configuration - connection to master will
// be returned. If shutdown was initiated - it will return error.
func (b *Base) GetConnection() (*sqlx.DB, error) {
	if b.areWeShuttingDown() {
		return nil, errors.ErrShutdownInProgress
	}

	slaveConnsCount := len(b.slaveConns)

	if slaveConnsCount == 0 {
		b.logger.Debug().Msg("There are no connections to slave, returning master connection")

		return b.GetMasterConnection()
	}

	var (
		validSlaveConnFound bool
		validSlaveConn      *sqlx.DB

		iterations int
	)

	b.logger.Debug().Int("slave connections", slaveConnsCount).Msg("Got slave connections, selecting one of them")

	for {
		// Wait 5 seconds maximum.
		// ToDo: configurable?
		if iterations > 25 {
			break
		}

		if slaveConnsCount == 1 && b.slaveConns[0].Ping() == nil {
			validSlaveConn = b.slaveConns[0]
			validSlaveConnFound = true

			break
		}

		conn := b.slaveConns[b.randomer.Intn(slaveConnsCount-1)]

		if conn != nil && conn.Ping() == nil {
			validSlaveConnFound = true
			validSlaveConn = conn
		}

		if validSlaveConnFound {
			break
		}

		b.logger.Debug().Msg("No valid slave connection was found, waiting for 200ms to appear")
		iterations++

		time.Sleep(time.Millisecond * 200)
	}

	if !validSlaveConnFound {
		return nil, errors.ErrDatabaseNoUsableConnections
	}

	return validSlaveConn, nil
}

// GetDialect returns dialect name for provider. Should be set with SetDialect() first.
func (b *Base) GetDialect() string {
	return b.dialect
}

// GetLogger returns logger which was initialized in InitializeBase().
func (b *Base) GetLogger() zl.Logger {
	return b.logger
}

// GetMasterConnection returns master connection pointer for later use. It
// should be used only for writing or updating, e.g. database migrations.
func (b *Base) GetMasterConnection() (*sqlx.DB, error) {
	if b.areWeShuttingDown() {
		return nil, errors.ErrShutdownInProgress
	}

	var iterations int

	for {
		// Wait 5 seconds maximum.
		// ToDo: configurable?
		if iterations > 25 {
			break
		}

		if b.masterConn != nil {
			break
		}

		b.logger.Debug().Msg("No valid master connection was found, waiting for 200ms to appear")
		iterations++

		time.Sleep(time.Millisecond * 200)
	}

	if b.masterConn == nil {
		return nil, errors.ErrDatabaseNoUsableConnections
	}

	return b.masterConn, nil
}

// GetName returns provider's name. Should be set with SetName() first.
func (b *Base) GetName() string {
	return b.name
}

// GetTransaction returns SQL transaction thing using connection to master.
func (b *Base) GetTransaction() (*sqlx.Tx, error) {
	conn, err := b.GetMasterConnection()
	if err != nil {
		return nil, fmt.Errorf("database provider: get transaction: get master connection: %w", err)
	}

	tx, err := conn.Beginx()
	if err != nil {
		b.logger.Error().Err(err).Msg("Failed to start SQL transaction")

		return nil, fmt.Errorf("database provider: get transaction: start new connection: %w", err)
	}

	return tx, nil
}

// InitializeBase initializes base parts of every database provider. Should be
// called after SetApplication() and SetName() as it will initialize logger which
// is taken from framework.Application and has named as passed to SetName().
func (b *Base) InitializeBase() error {
	logProvider, err := b.app.Logger.GetProvider(logging.DefaultLoggingProvider)
	if err != nil {
		return err
	}

	logger, err := logProvider.GetLogger(b.GetDialect()+"_"+b.GetName(), map[string]string{"name": b.name})
	if err != nil {
		return err
	}

	b.logger = logger.(zl.Logger)

	b.logger.Info().Msg("Initializing provider...")

	b.slaveConns = make([]*sqlx.DB, 0)

	// Linter whining here about using non-secure random number generator, but
	// this isn't about security, so:
	// nolint
	b.randomer = rand.New(rand.NewSource(time.Now().UnixNano()))

	return nil
}

// SetApplication sets application pointer.
func (b *Base) SetApplication(app *framework.Application) {
	b.app = app
}

// SetConfig sets configuration for base provider.
func (b *Base) SetConfig(config interface{}) error {
	cfg, ok := config.(Configurationer)
	if !ok {
		return errors.ErrNotAConfigurationStruct
	}

	b.config = cfg

	return b.config.BaseValidate()
}

// SetDialect sets dialect name for provider. This can be obtained using GetDialect()
// when using things like database migrations tools and so on.
func (b *Base) SetDialect(dialect string) {
	b.dialect = dialect
}

// SetName sets provider's name as it will be shown e.g. in logs.
func (b *Base) SetName(name string) {
	b.name = name
}

// Start starts provider work. This is blocking call and will
// return only if provider starting sequence will be successfully
// completed or completely failed.
func (b *Base) Start() error {
	if b.masterConn != nil {
		b.logger.Warn().Msg("Connection pointer isn't nil - trying to close presumably opened connection")

		err := b.masterConn.Close()
		if err != nil {
			b.logger.Error().Err(err).Msg("Failed to close database connection")
		}
	}

	// Establish connection to master server for non-read-only connections.
	if !b.config.IsReadOnly() {
		conn, err := b.startConn(b.config.GetMasterDSN())
		if err != nil {
			b.logger.Error().
				Err(err).
				Str("dsn", b.config.GetMasterDSN()).
				Msg("Failed to establish connection to master database")

			return err
		}

		b.logger.Debug().Msg("Master connection established")

		b.masterConn = conn
	}

	// Establish connections to slaves.
	// As this is blocking operation and done on initialization there is no mutex here.
	for _, dsn := range b.config.GetSlaveDSNs() {
		conn, err := b.startConn(dsn)
		if err != nil {
			b.logger.Error().Err(err).Str("dsn", dsn).Msg("Failed to establish connection to slave database")

			return err
		}

		b.logger.Debug().Str("dsn", dsn).Msg("Slave connection established")

		b.slaveConns = append(b.slaveConns, conn)
	}

	b.logger.Info().Msgf("%s %s database connections established", b.GetName(), b.GetDialect())

	return nil
}

// Really starts connection.
func (b *Base) startConn(dsn string) (*sqlx.DB, error) {
	conn, err := sqlx.Connect(b.GetDialect(), dsn)
	if err != nil {
		return nil, fmt.Errorf("database provider: start connection: %w", err)
	}

	conn.SetMaxIdleConns(b.config.GetMaxIdleConnections())
	conn.SetMaxOpenConns(b.config.GetMaxOpenedConnections())

	return conn, nil
}

// Stop stops provider from doing it's works. This is blocking call and
// will return only if provider stopping sequence will be successfully
// completed or completely failed.
func (b *Base) Stop() error {
	b.shouldShutdownMutex.Lock()
	b.shouldShutdown = true
	b.shouldShutdownMutex.Unlock()

	var errToReturn error

	for _, conn := range b.slaveConns {
		err := conn.Close()
		if err != nil {
			b.logger.Error().Err(err).Msg("Failed to close connection to slave database")

			errToReturn = err
		}
	}

	if b.masterConn != nil {
		err := b.masterConn.Close()
		if err != nil {
			b.logger.Error().Err(err).Msg("Failed to close connection to master database")

			errToReturn = err
		}
	}

	b.slaveConns = make([]*sqlx.DB, 0)

	return errToReturn
}
