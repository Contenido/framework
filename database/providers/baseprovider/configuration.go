package baseprovider

import "gitlab.com/Contenido/framework/errors"

// Configurationer is an interface that describes base provider configuration.
// This interface is accepted in SetBaseConfig so passed structure should conform.
type Configurationer interface {
	// BaseValidate validates base provider configuration.
	BaseValidate() error
	// GetMasterDSN returns master DSN string.
	GetMasterDSN() string
	// GetMaxIdleConnections returns maximum number of connections that can idle.
	GetMaxIdleConnections() int
	// GetMaxOpenedConnections returns maximum number of connections that can be opened.
	GetMaxOpenedConnections() int
	// GetSlaveDSNs returns slaves DSNs strings.
	GetSlaveDSNs() []string
	// IsReadOnly returns true if connection should be read-only.
	IsReadOnly() bool
}

// Configuration describes clickhouse connection configuration.
type Configuration struct {
	// MasterDSN is a DSN for connection to master server which will be
	// used for transactions and data writing/updating.
	MasterDSN string `json:"master_dsn"`
	// SlaveDSNs is a DSNs for connections to slave servers which will be
	// used for data reading.
	SlaveDSNs []string `json:"slave_dsns"`
	// MaxIdleConnections specifying maximum connections count that can idle.
	MaxIdleConnections int `json:"max_idle_connections"`
	// MaxOpenedConnections specifying maximum connections count that can be opened.
	MaxOpenedConnections int `json:"max_opened_connections"`
	// ReadOnly indicates that connection should be read-only. In that case
	// only slave connections will be returned.
	ReadOnly bool `json:"read_only"`
}

// BaseValidate validates base provider configuration.
func (c *Configuration) BaseValidate() error {
	// Empty DSN isn't allowed.
	// ToDo: check DSN for validity and push back error if DSN is invalid.
	// We should check it only for non-read-only connections, read-only
	// connections might have it emptied.
	if c.MasterDSN == "" && !c.ReadOnly {
		return errors.ErrDatabaseConfigurationMasterDSNInvalid
	}

	// If we'll try to establish read-only connection we should have slaves
	// defined as only these connections will be established.
	if c.ReadOnly && len(c.SlaveDSNs) == 0 {
		return errors.ErrDatabaseSlaveDSNsIsEmpty
	}

	// Connections counts with values less than 1 isn't allowed.
	if c.MaxIdleConnections < 1 || c.MaxOpenedConnections < 1 {
		return errors.ErrDatabaseConfigurationConnectionsCountInvalid
	}

	return nil
}

// GetMasterDSN returns master DSN string.
func (c *Configuration) GetMasterDSN() string {
	return c.MasterDSN
}

// GetMaxIdleConnections returns maximum number of connections that can idle.
func (c *Configuration) GetMaxIdleConnections() int {
	return c.MaxIdleConnections
}

// GetMaxOpenedConnections returns maximum number of connections that can be opened.
func (c *Configuration) GetMaxOpenedConnections() int {
	return c.MaxOpenedConnections
}

// GetSlaveDSNs returns slaves DSNs strings.
func (c *Configuration) GetSlaveDSNs() []string {
	return c.SlaveDSNs
}

// IsReadOnly returns true if connection should be read-only.
func (c *Configuration) IsReadOnly() bool {
	return c.ReadOnly
}
