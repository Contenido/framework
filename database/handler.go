package database

import (
	"fmt"

	"gitlab.com/Contenido/framework/errors"
	"gitlab.com/Contenido/framework/internal/handler"
)

// Handler handles all database things application will use. Database providers
// should be registered before application's business logic initialization
// in order to be properly used.
type Handler struct {
	handler.BaseHandler
}

// GetProvider returns provider by name.
func (h *Handler) GetProvider(providerName string) (Provider, error) {
	providerRaw, err := h.GetRawProvider(providerName)
	if err != nil {
		return nil, fmt.Errorf("database handler: %w", err)
	}

	provider, ok := providerRaw.(Provider)
	if !ok {
		return nil, errors.ErrInvalidProvider
	}

	return provider, nil
}

// Initialize initializes handler's internal state.
func (h *Handler) Initialize() {}
