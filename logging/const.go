package logging

const (
	// DefaultLoggingProvider is a logging provider that should be used by
	// default and should always be initialized.
	DefaultLoggingProvider = "zerolog"

	LogLevelDebug = "DEBUG"
	LogLevelInfo  = "INFO"
	LogLevelWarn  = "WARN"
	LogLevelError = "ERROR"
	LogLevelFatal = "FATAL"
)
