package zerolog

import (
	"log"
	"strings"

	"gitlab.com/Contenido/framework"
	"gitlab.com/Contenido/framework/internal/provider"
	"gitlab.com/Contenido/framework/logging"
	zl "github.com/rs/zerolog"
	zlog "github.com/rs/zerolog/log"
)

// Provider is a zerolog logging package provider.
type Provider struct {
	context    *framework.Application
	baseLogger *zl.Logger
	provider.BaseProvider
}

// NewProvider returns new zerolog provider.
func NewProvider(ctx *framework.Application) *Provider {
	// As this is a provider instantiation there is no actual possibility to fill all
	// fields (they'll be filled with Initialize() call), so:
	// nolint:exhaustivestruct
	return &Provider{
		context: ctx,
	}
}

// Configures logger on BaseProvider.Start() call.
func (p *Provider) configureLogger() error {
	p.setLoggerLevel(p.context.Configuration.Config.Framework.Logger.Level)

	return nil
}

// GetLogger returns a logger which was initialized with passed params.
// It might be a new one or previously created. Passed params should be
// map[string]string.
// ToDo: implement it.
func (p *Provider) GetLogger(loggerName string, params interface{}) (interface{}, error) {
	newLogger := p.baseLogger.With().Str("subsystem", strings.Replace(loggerName, " ", "_", -1)).Logger()

	parameters, ok := params.(map[string]string)
	if ok {
		for k, v := range parameters {
			newLogger = newLogger.With().Str(k, v).Logger()
		}
	}

	return newLogger, nil
}

// Initialize creates base logger.
func (p *Provider) Initialize() error {
	p.setLoggerLevel(logging.LogLevelDebug)

	bl := zlog.With().Logger()
	p.baseLogger = &bl

	p.RegisterStartFunc(p.configureLogger)

	return nil
}

// Log logs passed data using provided level and message. Also passed additionals
// map will be logged as separate field.
func (p *Provider) Log(level string, message string, additionals map[string]interface{}) {
	var event *zl.Event

	switch strings.ToUpper(level) {
	case logging.LogLevelDebug:
		event = p.baseLogger.Debug()
	case logging.LogLevelInfo:
		event = p.baseLogger.Info()
	case logging.LogLevelWarn:
		event = p.baseLogger.Warn()
	case logging.LogLevelError:
		event = p.baseLogger.Error()
	case logging.LogLevelFatal:
		event = p.baseLogger.Fatal()
	}

	if additionals != nil {
		event = event.Interface("additionals", additionals)
	}

	event.Msg(message)
}

// Logf is the very same as Log, but with formatting capabilities similar to fmt.Printf().
func (p *Provider) Logf(level string, template string, additionals map[string]interface{}, data ...interface{}) {
	var event *zl.Event

	switch strings.ToUpper(level) {
	case logging.LogLevelDebug:
		event = p.baseLogger.Debug()
	case logging.LogLevelInfo:
		event = p.baseLogger.Info()
	case logging.LogLevelWarn:
		event = p.baseLogger.Warn()
	case logging.LogLevelError:
		event = p.baseLogger.Error()
	case logging.LogLevelFatal:
		event = p.baseLogger.Fatal()
	}

	if additionals != nil {
		event = event.Interface("additionals", additionals)
	}

	event.Msgf(template, data...)
}

// SetConfig sets configuration. Normally. No-op here for now.
func (p *Provider) SetConfig(config interface{}) error { return nil }

// Sets logging level.
func (p *Provider) setLoggerLevel(level string) {
	if p.baseLogger != nil {
		p.baseLogger.Warn().Str("desired_level", level).Msg("Setting logger level")
	}

	switch strings.ToUpper(level) {
	case logging.LogLevelDebug:
		zl.SetGlobalLevel(zl.DebugLevel)
	case logging.LogLevelInfo:
		zl.SetGlobalLevel(zl.InfoLevel)
	case logging.LogLevelWarn:
		zl.SetGlobalLevel(zl.WarnLevel)
	case logging.LogLevelError:
		zl.SetGlobalLevel(zl.ErrorLevel)
	case logging.LogLevelFatal:
		zl.SetGlobalLevel(zl.FatalLevel)
	default:
		if p.baseLogger != nil {
			p.baseLogger.Error().Str("desired_level", level).Msg("Invalid logging level passed, forcing INFO")
		} else {
			log.Println("Invalid logger level passed:", level)
			log.Println("Forcing INFO")
		}

		zl.SetGlobalLevel(zl.InfoLevel)
	}
}
