package logging

import (
	"fmt"
	"log"

	"gitlab.com/Contenido/framework/errors"
	"gitlab.com/Contenido/framework/internal/handler"
)

// Handler is a logging handler.
type Handler struct {
	handler.BaseHandler
}

// GetProvider returns provider by name.
func (h *Handler) GetProvider(providerName string) (Provider, error) {
	providerRaw, err := h.GetRawProvider(providerName)
	if err != nil {
		return nil, fmt.Errorf("logging provider: %w", err)
	}

	provider, ok := providerRaw.(Provider)
	if !ok {
		return nil, errors.ErrInvalidProvider
	}

	return provider, nil
}

// Log logs a line using specified provider. Identical to log.Println().
func (h *Handler) Log(providerName, level, message string, additionals map[string]interface{}) {
	providerRaw, err := h.GetProvider(providerName)
	if err != nil {
		log.Println("Failed to get logging provider: " + err.Error() + ". Log line is ignored.")

		return
	}

	provider, ok := providerRaw.(Provider)
	if !ok {
		log.Println("Failed to get logging provider: isn't a logging provider " +
			"(does not conform to logging's Provider interface). Log line is ignored.")

		return
	}

	provider.Log(level, message, additionals)
}

// Logf logs a line using specified provider and format. Identical to log.Printf().
func (h *Handler) Logf(providerName, level, template string, additionals map[string]interface{}, data ...interface{}) {
	providerRaw, err := h.GetProvider(providerName)
	if err != nil {
		log.Println("Failed to get logging provider: " + err.Error() + ". Log line is ignored.")

		return
	}

	provider, ok := providerRaw.(Provider)
	if !ok {
		log.Println("Failed to get logging provider: isn't a logging provider " +
			"(does not conform to logging's Provider interface). Log line is ignored.")

		return
	}

	provider.Logf(level, template, additionals, data...)
}

// Initialize initializes handler's internal state.
func (h *Handler) Initialize() {}
