package logging

import "gitlab.com/Contenido/framework/internal/provider"

type Provider interface {
	provider.BaseInterface

	// GetLogger returns a logger which was initialized with passed params.
	// It might be a new one or previously created.
	GetLogger(loggerName string, params interface{}) (interface{}, error)
	// Log logs passed data using provided level and message. Also passed
	// additionals map will be logged as separate field. This function will
	// use reference logger, use GetLogger to create new logger or re-use
	// existing one.
	Log(level string, message string, additionals map[string]interface{})
	// Logf is the very same as Log, but with formatting capabilities
	// similar to fmt.Printf(). This function will use reference logger,
	// use GetLogger to create new logger or re-use existing one.
	Logf(level string, template string, additionals map[string]interface{}, data ...interface{})
}
