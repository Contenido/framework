package framework

import (
	"context"
	"flag"
	"fmt"

	"gitlab.com/Contenido/framework/communication"
	"gitlab.com/Contenido/framework/configuration"
	"gitlab.com/Contenido/framework/database"
	"gitlab.com/Contenido/framework/internal/handler"
	"gitlab.com/Contenido/framework/logging"
	"gitlab.com/Contenido/framework/pubsub"
	"gitlab.com/Contenido/framework/tasks"
)

// Application is an application-wide thing which gives ability to use
// base things like database access or communication channels to whole
// application.
type Application struct {
	Communication communication.Handler
	Configuration configuration.Handler
	Database      database.Handler
	Logger        logging.Handler
	PubSub        pubsub.Handler
	Tasks         tasks.Handler

	globalCtx context.Context
}

func NewApplication() *Application {
	c := &Application{
		Configuration: configuration.Handler{BaseHandler: handler.BaseHandler{}, Config: nil},
		Communication: communication.Handler{BaseHandler: handler.BaseHandler{}},
		Database:      database.Handler{BaseHandler: handler.BaseHandler{}},
		Logger:        logging.Handler{BaseHandler: handler.BaseHandler{}},
		PubSub:        pubsub.Handler{BaseHandler: handler.BaseHandler{}},
		Tasks:         tasks.Handler{BaseHandler: handler.BaseHandler{}},
	}
	c.initialize()

	return c
}

// GetContext returns global application context. This function should be called
// AFTER InitializePost() otherwise nil context may be returned.
func (c *Application) GetContext() context.Context {
	return c.globalCtx
}

// Initializes Application with base things.
func (c *Application) initialize() {
	c.Configuration.Initialize()
	c.Communication.Initialize()
	c.Logger.Initialize()
	c.Database.Initialize()
	c.PubSub.Initialize()
	c.Tasks.Initialize()
}

// InitializePost calls all InitializePost functions for all subsystems.
func (c *Application) InitializePost() (err error) {
	// Set context everywhere if passed. If not - create own context.
	if c.globalCtx == nil {
		c.globalCtx = context.Background()
	}

	c.Communication.SetContext(c.globalCtx)
	c.Configuration.SetContext(c.globalCtx)
	c.Database.SetContext(c.globalCtx)
	c.Logger.SetContext(c.globalCtx)
	c.PubSub.SetContext(c.globalCtx)
	c.Tasks.SetContext(c.globalCtx)

	// Clean shutdown defer.
	defer func() {
		if err != nil {
			if errs := c.Shutdown(); errs != nil {
				c.Logger.Logf(logging.DefaultLoggingProvider, "error", "Failed to stop subsystem providers: %s", nil, err.Error())
			}
		}
	}()

	// While we don't care about initialization sequence for InitializePre(),
	// InitializePost() things should be called in proper order:
	//
	// 0. CLI flags parsing.
	// 1. Configuration - because logger might be reconfigured.
	// 2. Logger - because we might need to reconfigure it (level, hooks, etc.).
	// 3. All other things - because they will definitely want first two
	//    things already configured.
	flag.Parse()

	err = c.Configuration.StartAll()
	if err != nil {
		c.Logger.Logf(logging.DefaultLoggingProvider,
			"error",
			"Failed to start configuration providers: %s",
			nil,
			err.Error(),
		)

		return fmt.Errorf("application: %w", err)
	}

	err = c.Logger.StartAll()
	if err != nil {
		c.Logger.Logf(logging.DefaultLoggingProvider, "error", "Failed to start logging providers: %s", nil, err.Error())

		return fmt.Errorf("application: %w", err)
	}

	err = c.PubSub.StartAll()
	if err != nil {
		c.Logger.Logf(logging.DefaultLoggingProvider, "error", "Failed to start pubsub providers: %s", nil, err.Error())

		return fmt.Errorf("application: %w", err)
	}

	err = c.Tasks.StartAll()
	if err != nil {
		c.Logger.Logf(logging.DefaultLoggingProvider, "error", "Failed to start tasks providers: %s", nil, err.Error())

		return fmt.Errorf("application: %w", err)
	}

	return nil
}

// SetContext sets global context that will be used in all providers where possible.
// This function should be called BEFORE InitializePost().
func (c *Application) SetContext(ctx context.Context) {
	c.globalCtx = ctx
}

// Shutdown shutdowns (or stops) everything we have initialized.
func (c *Application) Shutdown() []error {
	var errs []error

	errs = append(errs, c.Communication.StopAll()...)
	errs = append(errs, c.Configuration.StopAll()...)
	errs = append(errs, c.Database.StopAll()...)
	errs = append(errs, c.Logger.StopAll()...)
	errs = append(errs, c.Tasks.StopAll()...)
	errs = append(errs, c.PubSub.StopAll()...)

	return errs
}
