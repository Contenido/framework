package utils

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

// NormalizeFilePath normalizes file path by replacing commonly used
// path identifiers (like "~") with actual values.
func NormalizeFilePath(filePath string) (string, error) {
	if strings.Contains(filePath, "~") {
		homePath, found := os.LookupEnv("HOME")
		if found {
			filePath = strings.Replace(filePath, "~", homePath, 1)
		} else {
			// Doing like every shell will do.
			filePath = strings.Replace(filePath, "~", "", 1)
		}
	}

	absolutePath, err := filepath.Abs(filePath)
	if err != nil {
		return "", fmt.Errorf("utils.NormalizeFilePath: %w", err)
	}

	return absolutePath, nil
}
