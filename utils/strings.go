package utils

import (
	"math/rand"
	"strconv"
	"time"
)

const letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"

// nolint
func init() {
	rand.Seed(time.Now().UnixNano())
}

// GenerateRandomString generates random string with designated length.
func GenerateRandomString(length int) string {
	b := make([]byte, length)

	for i := range b {
		// ToDo: migrate to crypto/rand?
		// nolint
		b[i] = letters[rand.Int63()%int64(len(letters))]
	}

	return string(b)
}

func Int64SliceToStringSlice(ints []int64) []string {
	strs := make([]string, 0, len(ints))
	for _, num := range ints {
		strs = append(strs, strconv.FormatInt(num, 10))
	}

	return strs
}
